﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BusinessLayer
{
    [MetadataType(typeof(CustomersMetaData))]
    public partial class Customers
    {
        //[NotMapped]
        //[DisplayName("Confirm Password")]
        //[Required(ErrorMessage = "Confirm Password is required")]
        //[Compare("Password", ErrorMessage = "Password Mismatch")]
        //[DataType(DataType.Password)]
        //public string ConfirmPassword { get; set; }
    }

    public class CustomersMetaData
    {
        [DisplayName("Customer Name")]
        [Required(ErrorMessage = "Name is required")]
        public string CustomerName { get; set; }

        [DisplayName("Email")]
        [Required(ErrorMessage = "Email is required")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Invalid email format")]
        public string Email { get; set; }

        [DisplayName("Mobile")]
        [Required(ErrorMessage = "Mobile is required")]
        public string Phone { get; set; }

        [DisplayName("Gender")]
        [Required(ErrorMessage = "Gender is required")]
        public byte Gender { get; set; }

        [DisplayName("Password")]
        [Required(ErrorMessage = "Password is required")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        //[DisplayName("Pin Code")]
        //[Required(ErrorMessage = "Please Enter your Pin Code")]
        //[StringLength(maximumLength: 4, MinimumLength = 4, ErrorMessage = "Pin Code must be 4 digits")]
        //public string Pin { get; set; } 

        [DisplayName("Nationality")]
        [Required(ErrorMessage = "Nationality is required")]
        public int FKCountryID { get; set; }
    }
}
