﻿using System.Collections.Generic;

namespace BusinessLayer
{
    public partial class StoresView
    {
        public IEnumerable<StoresProductsView> Products { get; set; }

        public IEnumerable<StoresWorkingHoursView> WorkingHours { get; set; }

        public IEnumerable<StoresOffersView> Offers { get; set; }

        public bool IsCustomerFavorite { get; set; }

        public IEnumerable<string> Gallery { get; set; }
    }
}
