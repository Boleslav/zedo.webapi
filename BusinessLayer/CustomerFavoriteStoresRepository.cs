﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLayer
{
    public class CustomerFavoriteStoresRepository : RepositoryBase<CustomerFavoriteStores>
    {
        public bool RemoveByCustomerStore(long customerId, int storeId)
        {
            using (var dbContext = ZedoContext())
            {
                try
                {
                    var entity = dbContext.CustomerFavoriteStores.FirstOrDefault(e => e.FKCustomerID == customerId && e.FKStoreID == storeId);
                    if (entity != null)
                    {
                        dbContext.CustomerFavoriteStores.Remove(entity);
                        int value = dbContext.SaveChanges();
                        return value > 0;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
                return false;
            }
        }
    }
}
