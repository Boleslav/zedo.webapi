﻿using System.Collections.Generic;
using System.Linq;

namespace BusinessLayer.Common
{
    public class GenericResult
    {
        public bool Status => !MessagesList.Any();
        public object Value { get; set; }
        public List<string> MessagesList { get; }

        public GenericResult()
        {
            MessagesList = new List<string>(); 
        }

        public void AddMessage(string message)
        {
            MessagesList.Add(message);
        }

        public void AddMessages(IEnumerable<string> messages)
        {
            MessagesList.AddRange(messages);
        }
    }

}
