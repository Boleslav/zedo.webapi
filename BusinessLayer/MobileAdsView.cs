//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BusinessLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class MobileAdsView
    {
        public int AdsID { get; set; }
        public string AdLink { get; set; }
        public string AdImage { get; set; }
        public MobilePages MobilePageID { get; set; }
    }
}
