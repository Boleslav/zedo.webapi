﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.ComponentModel;
using System.Data.Entity;
using X.PagedList;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations;
using BusinessLayer.Common;

namespace BusinessLayer
{
    [DataObject(true)]

    public abstract class RepositoryBase<TEntity> where TEntity : class
    {
        public ZedoDBEntities ZedoContext()
        {
            var context = new ZedoDBEntities();
            context.Configuration.LazyLoadingEnabled = false;
            context.Configuration.ProxyCreationEnabled = false;
            return context;
        }

        #region CRUD

        /// <summary>
        /// Add New Entity to the Context
        /// Require calling Save() method add the entity to the DB
        /// </summary>
        /// <param name="entity">Entity to add</param>
        public GenericResult Add(TEntity entity)
        {
            using (var dbContext = ZedoContext())
            {
                var result = new GenericResult();
                try
                {
                    dbContext.Set<TEntity>().Add(entity);
                    int value = dbContext.SaveChanges();
                    if (value > 0)
                    {
                        result.Value = value;
                    }
                }
                catch (Exception ex)
                {
                    result.AddMessage(ex.Message);
                    result.Value = ex;
                }
                return result;
            }
        }

        /// <summary>
        /// Add list of entities to the Context
        /// Require calling Save() method add the entites to the DB
        /// </summary>
        /// <param name="entities">List of entities to add</param>
        public GenericResult AddRange(IEnumerable<TEntity> entities)
        {
            using (var dbContext = ZedoContext())
            {
                var result = new GenericResult();
                try
                {
                    dbContext.Set<TEntity>().AddRange(entities);
                    int value = dbContext.SaveChanges();
                    if (value > 0)
                    {
                        result.Value = value;
                    }
                }
                catch (Exception ex)
                {
                    result.AddMessage(ex.Message);
                    result.Value = ex;
                }
                return result;
            }
        }

        /// <summary>
        /// Attach entity to the context to update it
        /// Require calling Save() method update the entity in the DB
        /// </summary>
        /// <param name="entity">Entity to update</param>
        public GenericResult Update(TEntity entity)
        {
            using (var dbContext = ZedoContext())
            {
                var result = new GenericResult();
                try
                {
                    dbContext.Set<TEntity>().Attach(entity);
                    dbContext.Entry<TEntity>(entity).State = EntityState.Modified;
                    int value = dbContext.SaveChanges();
                    if (value > 0)
                    {
                        result.Value = value;
                    }
                }
                catch (Exception ex)
                {
                    result.AddMessage(ex.Message);
                    result.Value = ex;
                }
                return result;
            }
        }

        /// <summary>
        /// Remove entity by Id
        /// Require calling Save() method remove the entity from the DB
        /// </summary>
        /// <param name="id">Id of entity to remove it</param>
        public GenericResult Remove(object id)
        {
            using (var dbContext = ZedoContext())
            {
                var result = new GenericResult();
                try
                {
                    var entity = dbContext.Set<TEntity>().Find(id);
                    dbContext.Set<TEntity>().Remove(entity);
                    int value = dbContext.SaveChanges();
                    if (value > 0)
                    {
                        result.Value = value;
                    }
                }
                catch (Exception ex)
                {
                    result.AddMessage(ex.Message);
                    result.Value = ex;
                }
                return result;
            }
        }

        /// <summary>
        /// Remove entity by entity object 
        /// Require calling Save() method remove the entity from the DB
        /// </summary>
        /// <param name="entity">Entity to remove</param>
        public GenericResult Remove(TEntity entity)
        {
            using (var dbContext = ZedoContext())
            {
                var result = new GenericResult();
                try
                {
                    dbContext.Set<TEntity>().Remove(entity);
                    int value = dbContext.SaveChanges();
                    if (value > 0)
                    {
                        result.Value = value;
                    }
                }
                catch (Exception ex)
                {
                    result.AddMessage(ex.Message);
                    result.Value = ex;
                }
                return result;
            }
        }

        /// <summary>
        /// Remove entities list by entities list object 
        /// Require calling Save() method remove the entities from the DB
        /// </summary>
        /// <param name="entities">Entities list to remove</param>
        public GenericResult RemoveRange(IEnumerable<TEntity> entities)
        {
            using (var dbContext = ZedoContext())
            {
                var result = new GenericResult();
                try
                {
                    dbContext.Set<TEntity>().RemoveRange(entities);
                    int value = dbContext.SaveChanges();
                    if (value > 0)
                    {
                        result.Value = value;
                    }
                }
                catch (Exception ex)
                {
                    result.AddMessage(ex.Message);
                    result.Value = ex;
                }
                return result;
            }
        }

        #endregion

        #region Sync Methods

        /// <summary>
        /// Get entity by Id
        /// </summary>
        /// <param name="id">Id of the entity to get</param>
        /// <returns>Entity that match the Id</returns>
        public TEntity GetById<TKey>(TKey id)
        {
            using (var dbContext = ZedoContext())
            {
                return dbContext.Set<TEntity>().Find(id);
            }
        }

        /// <summary>
        /// Get all entites
        /// </summary>
        /// <returns>List of all entites</returns>
        public virtual List<TEntity> GetAll()
        {
            using (var dbContext = ZedoContext())
            {
                return dbContext.Set<TEntity>().ToList();
            }
        }
        /// <summary>
        /// Inclue multiple
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="include"></param>
        /// <returns></returns>
        public List<TEntity> GetWithInclude(Expression<Func<TEntity, bool>> predicate, params string[] include)
        {
            using (var dbContext = ZedoContext())
            {
                IQueryable<TEntity> query = dbContext.Set<TEntity>();
                query = include.Aggregate(query, (current, inc) => current.Include(inc));
                return query.Where(predicate).ToList();
            }
        }

        /// <summary>
        /// Get pages list of entities
        /// </summary>
        /// <typeparam name="TKey">Type of order field</typeparam>
        /// <param name="orderyBy">Order by clause</param>
        /// <param name="pageIndex">Page index to get</param>
        /// <param name="pageSize">Page size Default : 10</param>
        /// <param name="isAscendingOrder">Indicate order type ascending or descending</param>
        /// <returns>List of entites that match order by, page index and page size</returns>
        public IPagedList<TEntity> GetPaged<TKey>(Expression<Func<TEntity, TKey>> orderyBy, int pageIndex = 1, int pageSize = 10, bool isAscendingOrder = true)
        {
            using (var dbContext = ZedoContext())
            {
                return isAscendingOrder
                      ? dbContext.Set<TEntity>().OrderBy(orderyBy).ToPagedList(pageIndex, pageSize)
                      : dbContext.Set<TEntity>().OrderByDescending(orderyBy).ToPagedList(pageIndex, pageSize);
            }
        }

        /// <summary>
        /// Get pages list of entities
        /// </summary>
        /// <typeparam name="TKey">Type of order field</typeparam>
        /// <param name="predicate">Where clause</param>
        /// <param name="orderyBy">Order by clause</param>
        /// <param name="pageIndex">Page index to get</param>
        /// <param name="pageSize">Page size Default : 10</param>
        /// <param name="isAscendingOrder">Indicate order type ascending or descending</param>
        /// <returns>List of entites that match order by, page index and page size</returns>
        public IPagedList<TEntity> GetPaged<TKey>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, TKey>> orderyBy, int pageIndex = 1, int pageSize = 10, bool isAscendingOrder = true)
        {
            using (var dbContext = ZedoContext())
            {
                return isAscendingOrder
                   ? dbContext.Set<TEntity>().Where(predicate).OrderBy(orderyBy).ToPagedList(pageIndex, pageSize)
                   : dbContext.Set<TEntity>().Where(predicate).OrderByDescending(orderyBy).ToPagedList(pageIndex, pageSize);
            }
        }

        /// <summary>
        /// Find entity that match search criteria
        /// </summary>
        /// <param name="predicate">Where clause</param>
        /// <returns>entity that match search criteria</returns>
        public TEntity Find(Expression<Func<TEntity, bool>> predicate)
        {
            using (var dbContext = ZedoContext())
            {
                return dbContext.Set<TEntity>().FirstOrDefault(predicate);
            }
        }

        /// <summary>
        /// Find list of entities that match search criteria
        /// </summary>
        /// <param name="predicate">Where clause</param>
        /// <returns>List of entities that match search criteria</returns>
        public virtual List<TEntity> FindAll(Expression<Func<TEntity, bool>> predicate)
        {
            using (var dbContext = ZedoContext())
            {
                return dbContext.Set<TEntity>().Where(predicate).ToList();
            }
        }

        /// <summary>
        /// Find list of ordered entities that match search criteria
        /// </summary>
        /// <typeparam name="TKey">Type of order field</typeparam>
        /// <param name="predicate">Where clause</param>
        /// <param name="orderyBy">Order by clause</param>
        /// <param name="isAscendingOrder"></param>
        /// <returns>List of ordered entities that match search criteria</returns>
        public List<TEntity> FindAll<TKey>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, TKey>> orderyBy, bool isAscendingOrder = true)
        {
            using (var dbContext = ZedoContext())
            {
                return isAscendingOrder
                 ? dbContext.Set<TEntity>().AsQueryable().Where(predicate).OrderBy(orderyBy).ToList()
                 : dbContext.Set<TEntity>().AsQueryable().Where(predicate).OrderByDescending(orderyBy).ToList();
            }
        }

        /// <summary>
        /// Get total count for rows in Db
        /// </summary>
        /// <returns>Int for total count for rows in Db</returns>
        public int GetCount()
        {
            using (var dbContext = ZedoContext())
            {
                return dbContext.Set<TEntity>().Count();
            }
        }

        /// <summary>
        ///  Get total count for rows in Db that match search criteria
        /// </summary>
        /// <param name="predicate">Where clause</param>
        /// <returns>Int for total count for rows in Db that match search criteria</returns>
        public int GetCount(Expression<Func<TEntity, bool>> predicate)
        {
            using (var dbContext = ZedoContext())
            {
                return dbContext.Set<TEntity>().Count(predicate);
            }
        }

        #endregion 
    }
}

