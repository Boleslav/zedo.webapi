﻿using System.Collections.Generic;

namespace BusinessLayer
{
    public class StoresSearch
    {
        public StoresSearch()
        {
            MallsIds = new List<int>();
            LocationsIds = new List<int>();
            ProductCategoriesIds = new List<int>();
            BrandsIds = new List<int>();
        }

        public string SearchText { get; set; }
        public bool NewOffersOnly { get; set; }
        public List<int> MallsIds { get; set; }
        public List<int> LocationsIds { get; set; }
        public List<int> ProductCategoriesIds { get; set; }
        public List<int> BrandsIds { get; set; }
    }
}
