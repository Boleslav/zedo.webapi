﻿using System.Linq;

namespace BusinessLayer
{
    public class MobileAdsViewRepository : RepositoryBase<MobileAdsView>
    {
        public MobileAdsView GetByMobilePage(MobilePages mobilePage)
        {
            using (var dbContext = ZedoContext())
            {
                return dbContext.MobileAdsView.FirstOrDefault(e => e.MobilePageID == mobilePage);
            }
        }
    }
}
