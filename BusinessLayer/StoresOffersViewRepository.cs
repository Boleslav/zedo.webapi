﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace BusinessLayer
{
    public class StoresOffersViewRepository : RepositoryBase<StoresOffersView>
    {
        public override List<StoresOffersView> FindAll(Expression<Func<StoresOffersView, bool>> predicate)
        {
            using (var dbContext = ZedoContext())
            {
                return dbContext.StoresOffersView.Where(predicate)
                                                 .OrderBy(e => e.FkStoreID)
                                                 .ThenBy(e => e.IsYearly).ToList();
            }
        }

        public List<StoresOffersView> GetCurrentYearRedeemed(long customerId)
        {
            using (var dbContext = ZedoContext())
            {
                int year = DateTime.Now.Date.Year;
                var couponIds = dbContext.RedeemCoupons.Where(e => e.FkCustomerID == customerId && e.RedeemDate.Year == year).Select(e => e.FkStoreOfferID);
                var list = dbContext.StoresOffersView.Where(e => couponIds.Contains(e.StoreCouponID)).OrderBy(e => e.FkStoreID).ToList();
                list.ForEach(e => e.IsRedeemed = true);
                return list;
            }
        }
    }
}
