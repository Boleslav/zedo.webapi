﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace BusinessLayer
{
    public class StoresViewRepository : RepositoryBase<StoresView>
    {
        public StoresView GetWithDetails(int storeId, long? customerId = null)
        {
            using (var dbContext = ZedoContext())
            {
                StoresView store;
                var command = dbContext.Database.Connection.CreateCommand();
                command.CommandText = "[dbo].[GetStoreDetailsProc]";
                command.CommandType = CommandType.StoredProcedure;

                var parameter = command.CreateParameter();
                parameter.ParameterName = "@StoreId";
                parameter.Value = storeId;
                command.Parameters.Add(parameter);

                try
                {
                    dbContext.Database.Connection.Open();
                    var reader = command.ExecuteReader();

                    store = ((IObjectContextAdapter)dbContext).ObjectContext.Translate<StoresView>(reader, "StoresView", MergeOption.AppendOnly).FirstOrDefault();

                    reader.NextResult();
                    store.Gallery = ((IObjectContextAdapter)dbContext).ObjectContext.Translate<StoreGalleryView>(reader, "StoreGalleryView", MergeOption.AppendOnly).Select(e => e.GalleryImage).ToList();

                    reader.NextResult();
                    store.WorkingHours = ((IObjectContextAdapter)dbContext).ObjectContext.Translate<StoresWorkingHoursView>(reader, "StoresWorkingHoursView", MergeOption.AppendOnly).ToList();

                    reader.NextResult();
                    store.Products = ((IObjectContextAdapter)dbContext).ObjectContext.Translate<StoresProductsView>(reader, "StoresProductsView", MergeOption.AppendOnly).ToList();

                    reader.NextResult();
                    store.Offers = ((IObjectContextAdapter)dbContext).ObjectContext.Translate<StoresOffersView>(reader, "StoresOffersView", MergeOption.AppendOnly).ToList();

                    if (customerId.HasValue)
                    {
                        var storesIdsList = dbContext.CustomerFavoriteStores.Where(e => e.FKCustomerID == customerId).Select(e => e.FKStoreID);
                        store.IsCustomerFavorite = storesIdsList.Contains(store.StoreID);

                        var storeOffersIdsList = dbContext.RedeemCoupons.Where(e => e.FkCustomerID == customerId).Select(e => e.FkStoreOfferID);
                        if (storeOffersIdsList.Any())
                        {
                            store.Offers.ToList().ForEach(e => e.IsRedeemed = storeOffersIdsList.Contains(e.StoreCouponID));
                        }
                    }
                }
                finally
                {
                    dbContext.Database.Connection.Close();
                }
                return store;
            }
        }

        //public StoresView GetByCustomer(int storeId, long customerId)
        //{
        //    using (var dbContext = ZedoContext())
        //    {
        //        var store = new StoresView();
        //        var result = dbContext.StoresView.Where(e => e.StoreID == storeId)
        //                              .GroupJoin(dbContext.StoreGalleryView,
        //                                       s => s.StoreID,
        //                                       g => g.FkStoreID,
        //                                       (s, g) => new { Stroe = s, Gallery = g })
        //                              .SelectMany(s => s.Gallery.DefaultIfEmpty(), (s, g) => s)
        //                              .GroupJoin(dbContext.StoresWorkingHoursView,
        //                                       sg => sg.Stroe.StoreID,
        //                                       wh => wh.FkStoreId,
        //                                       (sg, wh) => new { SG = sg, WorkingHours = wh })
        //                              .SelectMany(sg => sg.WorkingHours.DefaultIfEmpty(), (sg, wh) => sg)
        //                              .GroupJoin(dbContext.StoresProductsView,
        //                                        sgwh => sgwh.SG.Stroe.StoreID,
        //                                        p => p.FkStoreID,
        //                                        (sgwh, p) => new { SGWH = sgwh, Product = p })
        //                              .SelectMany(sgwh => sgwh.Product.DefaultIfEmpty(), (sgwh, p) => sgwh)
        //                              .GroupJoin(dbContext.StoresOffersView,
        //                                    sgwhp => sgwhp.SGWH.SG.Stroe.StoreID,
        //                                    o => o.FkStoreID,
        //                                    (sgwhp, o) => new { SGWHP = sgwhp, Offers = o })
        //                              .SelectMany(swhp => swhp.Offers.DefaultIfEmpty(), (swhp, o) => swhp).FirstOrDefault();

        //        if (result != null)
        //        {
        //            store = result.SGWHP.SGWH.SG.Stroe;
        //            store.Gallery = result.SGWHP.SGWH.SG.Gallery.Select(e => e.GalleryImage);
        //            store.WorkingHours = result.SGWHP.SGWH.WorkingHours;
        //            store.Products = result.SGWHP.Product;
        //            store.Offers = result.Offers;

        //            var storesIdsList = dbContext.CustomerFavoriteStores.Where(e => e.FKCustomerID == customerId).Select(e => e.FKStoreID);
        //            store.IsCustomerFavorite = storesIdsList.Contains(store.StoreID);

        //            var storeOffersIdsList = dbContext.RedeemCoupons.Where(e => e.FkCustomerID == customerId).Select(e => e.FkStoreOfferID);
        //            if (storeOffersIdsList.Any())
        //            {
        //                store.Offers.ToList().ForEach(e => e.IsRedeemed = storeOffersIdsList.Contains(e.StoreCouponID));
        //            }
        //        }
        //        return store;
        //    }
        //}

        public List<StoresView> GetAllByMallIds(IEnumerable<int> ids)
        {
            using (var dbContext = ZedoContext())
            {
                var storesIdsList = dbContext.StoreMalls.Where(e => ids.Contains(e.FkMallID)).Select(e => e.FkStoreID);
                return dbContext.StoresView.Where(e => storesIdsList.Contains(e.StoreID)).ToList();
            }
        }

        public List<StoresView> GetAllByBrandIds(IEnumerable<int> ids)
        {
            using (var dbContext = ZedoContext())
            {
                var storesIdsList = dbContext.StoreBrands.Where(e => ids.Contains(e.FkBrandID)).Select(e => e.FkStoreID);
                return dbContext.StoresView.Where(e => storesIdsList.Contains(e.StoreID)).ToList();
            }
        }

        public List<StoresView> GetAllByProductCategoryIds(IEnumerable<int> ids)
        {
            using (var dbContext = ZedoContext())
            {
                var storesIdsList = dbContext.StoreProducts.Where(e => ids.Contains(e.FkProductCategoryID)).Select(e => e.FkStoreID);
                return dbContext.StoresView.Where(e => storesIdsList.Contains(e.StoreID)).ToList();
            }
        }

        public List<StoresView> GetAllByCustomerFavorites(long id)
        {
            using (var dbContext = ZedoContext())
            {
                var storesIdsList = dbContext.CustomerFavoriteStores.Where(e => e.FKCustomerID == id).Select(e => e.FKStoreID);
                return dbContext.StoresView.Where(e => storesIdsList.Contains(e.StoreID)).ToList();
            }
        }

        public List<StoresView> Search(StoresSearch search)
        {
            using (var dbContext = ZedoContext())
            {
                var query = dbContext.StoresView.AsQueryable();
                if (!string.IsNullOrEmpty(search.SearchText))
                {
                    var storeIds = dbContext.StoreMalls.Include("Malls").Where(e => e.Malls.NameEn.Contains(search.SearchText)).Select(e => e.FkStoreID);
                    storeIds = storeIds.Concat(dbContext.StoreBrands.Include("Brands").Where(e => e.Brands.NameEn.Contains(search.SearchText)).Select(e => e.FkStoreID));
                    storeIds = storeIds.Concat(dbContext.StoreProducts.Include("ProductCategories").Where(e => e.ProductCategories.NameEn.Contains(search.SearchText)).Select(e => e.FkStoreID));
                    storeIds = storeIds.Distinct();
                    query = query.Where(e => e.StoreName.Contains(search.SearchText) || storeIds.Contains(e.StoreID));
                }

                if (search.NewOffersOnly)
                {
                    var offers = dbContext.StoresOffersView.Where(e => !e.IsYearly).Select(e => e.FkStoreID);
                    query = query.Where(e => offers.Contains(e.StoreID));
                }

                if (search.MallsIds.Any())
                {
                    var malls = dbContext.StoreMalls.Where(e => search.MallsIds.Contains(e.FkMallID)).Select(e => e.FkStoreID);
                    query = query.Where(e => malls.Contains(e.StoreID));
                }

                if (search.LocationsIds.Any())
                {
                    query = query.Where(e => search.LocationsIds.Contains((int)e.LocationID));
                }

                if (search.ProductCategoriesIds.Any())
                {
                    var productCategories = dbContext.StoreProducts.Where(e => search.ProductCategoriesIds.Contains(e.FkProductCategoryID)).Select(e => e.FkStoreID);
                    query = query.Where(e => productCategories.Contains(e.StoreID));
                }

                if (search.BrandsIds.Any())
                {
                    var brands = dbContext.StoreBrands.Where(e => search.BrandsIds.Contains(e.FkBrandID)).Select(e => e.FkStoreID);
                    query = query.Where(e => brands.Contains(e.StoreID));
                }

                return query.ToList();
            }
        }
    }
}
