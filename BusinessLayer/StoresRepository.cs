﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class StoresRepository : RepositoryBase<Stores>
    {
        public List<Stores> GetAllByMall(int id)
        {
            using (var dbContext = ZedoContext())
            {
                return dbContext.StoreMalls
                    .Include("Stores")
                    .Include("StoreWorkTime")
                    .Include("Locations")
                    .Where(e => e.FkMallID == id).Select(e => e.Stores).ToList();
            }
        }

        public List<Stores> GetAllByBrand(int id)
        {
            using (var dbContext = ZedoContext())
            {
                return dbContext.StoreBrands
                    .Include("Brands")
                    .Include("StoreWorkTime")
                    .Include("Locations")
                    .Where(e => e.FkBrandID == id).Select(e => e.Stores).ToList();
            }
        }
    }
}
