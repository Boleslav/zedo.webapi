﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Http;
using BusinessLayer;

namespace ZedoWebApi.Controllers
{
    public class OffersController : BaseController
    {
        private readonly StoresOffersViewRepository _repository;
        public OffersController()
        {
            _repository = new StoresOffersViewRepository();
        }

        public object GetAll()
        {
            try
            {
                DateTime date = DateTime.Now.Date;
                var list = _repository.FindAll(e => date >= e.CouponFrom && date <= e.CouponTo);
                Result.Value = list;
                if (list == null || !list.Any())
                {
                    Result.AddMessage("No offers available");
                }
            }
            catch (Exception)
            {
                Result.AddMessage("No offers available");
            }
            return Result;
        }

        [Route("~/api/Offers/New/")]
        public object GetAllNew()
        {
            try
            {
                DateTime date = DateTime.Now.Date;
                var list = _repository.FindAll(e => !e.IsYearly && date >= e.CouponFrom && date <= e.CouponTo);
                Result.Value = list;
                if (list == null || !list.Any())
                {
                    Result.AddMessage("No offers available");
                }
            }
            catch (Exception)
            {
                Result.AddMessage("No offers available");
            }
            return Result;
        }

        [Route("~/api/Offers/Redeemed/{customerId}")]
        public object GetRedeemedHistory(long customerId)
        {
            try
            {
                var list = _repository.GetCurrentYearRedeemed(customerId);
                Result.Value = list;
                if (list == null || !list.Any())
                {
                    Result.AddMessage("No redeemed offers to display");
                }
            }
            catch (Exception)
            {
                Result.AddMessage("No redeemed offers to display");
            }
            return Result;
        }

        [HttpGet]
        [Route("~/api/Offers/coupons/{userId}/{offerId}")]
        public object RedeemCoupon(long userId, long offerId)
        {
            try
            {
                var repo = new RedeemCouponsRepository();
                var existingCoupon = repo.Find(x => x.FkCustomerID == userId && x.FkStoreOfferID == offerId);
                Result.Value = existingCoupon;
            }
            catch (Exception)
            {
                Result.AddMessage(SaveError);
            }
            return Result;
        }
        [HttpPost]
        [Route("~/api/Offers/coupons/")]
        public object RedeemCoupon([FromBody]RedeemCoupons entity)
        {
            try
            {
                var repo = new RedeemCouponsRepository();
                var existingCoupon = repo.FindAll(x => x.FkStoreOfferID == entity.FkStoreOfferID && x.FkCustomerID == entity.FkCustomerID).FirstOrDefault();
                if (existingCoupon == null)
                {
                    var storeOffers = new StoresOffersViewRepository();
                    var offer = storeOffers.Find(x => x.StoreCouponID == entity.FkStoreOfferID);
                    var storesRepo = new StoresViewRepository();
                    var store = storesRepo.Find(x => x.StoreID == offer.FkStoreID);

                    // todo: process store pin code check
//                    if (store.Pin != entity.PinStore)
//                    {
//                            Result.AddMessage("Incorrect pin.");
//                    }

                    // todo: to be sure multithreading won't break the itemm adding
                    entity.Ref = GenerateRedeemCode();
                    entity.RedeemDate = DateTime.UtcNow;
                    var result = new RedeemCouponsRepository().Add(entity);
                    if (result.Status)
                    {
                        //entity.PinCustomer = RandomString(7);
                        Result.Value = entity;
                        entity.RedeemID = (long)result.Value;
                    }
                    else
                    {
                        var ex = result.Value as Exception;
                        Result.AddMessage(SaveError + " > " + string.Join(" : ", result.MessagesList) + " >> " + ex?.GetBaseException()?.Message);
                    }
                }
                else
                {
                    Result.Value = existingCoupon;
                }
            }
            catch (Exception e)
            {
                Result.AddMessage(SaveError + " > " + e.Message + " > " + e.GetBaseException()?.Message);
            }
            return Result;
        }

        private string GenerateRedeemCode()
        {
            var charactersCount = 16;

            var result = new List<char>();
            var digitsSample = "";
            while (result.Count < charactersCount)
            {
                if (digitsSample.Length == 0)
                {
                    digitsSample = Guid.NewGuid().ToString("N");
                    digitsSample = Regex.Replace(digitsSample, "[^0-9]", "");
                }

                if (digitsSample.Length == 0)
                {
                    continue;
                }

                result.Add(digitsSample[0]);
                digitsSample = digitsSample.Substring(1);
            }

            return string.Join("", result);
        }
    }
}
