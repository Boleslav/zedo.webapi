﻿using System;
using System.Linq;
using BusinessLayer;

namespace ZedoWebApi.Controllers
{
    public class CountriesController : BaseController
    {
        private readonly CountriesRepository _repository;
        public CountriesController()
        {
            _repository = new CountriesRepository();
        }

        public object GetAll()
        {
            try
            {
                var list = _repository.GetAll();
                Result.Value = list;
                if (list == null || !list.Any())
                {
                    Result.AddMessage("No countries to display");
                }
            }
            catch (Exception)
            {
                Result.AddMessage("No countries to display");
            }
            return Result;
        }
    }
}
