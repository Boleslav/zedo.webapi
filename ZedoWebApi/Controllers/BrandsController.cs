﻿using System;
using System.Linq;
using BusinessLayer;

namespace ZedoWebApi.Controllers
{
    public class BrandsController : BaseController
    {
        private readonly BrandsViewRepository _repository;
        public BrandsController()
        {
            _repository = new BrandsViewRepository();
        }

        public object GetAll()
        {
            try
            {
                var list = _repository.GetAll();
                Result.Value = list;
                if (list == null || !list.Any())
                {
                    Result.AddMessage("No brands to display");
                }
            }
            catch (Exception)
            {
                Result.AddMessage("No brands to display");
            }
            return Result;
        }
    }
}
