﻿using System;
using System.Linq;
using BusinessLayer;

namespace ZedoWebApi.Controllers
{
    public class LocationsController : BaseController
    {
        private readonly LocationsRepository _repository;
        public LocationsController()
        {
            _repository = new LocationsRepository();
        }

        public object GetAll()
        {
            try
            {
                var list = _repository.FindAll(e => !e.IsRemoved);
                Result.Value = list;
                if (list == null || !list.Any())
                {
                    Result.AddMessage("No locations to display");
                }
            }
            catch (Exception)
            {
                Result.AddMessage("No locations to display");
            }
            return Result;
        }
    }
}
