﻿using System;
using System.Linq;
using BusinessLayer;

namespace ZedoWebApi.Controllers
{
    public class ProductCategoriesController : BaseController
    {
        readonly ProductCategoriesViewRepository _repository;
        public ProductCategoriesController()
        {
            _repository = new ProductCategoriesViewRepository();
        }

        public object GetAll()
        {
            try
            {
                var list = _repository.GetAll();
                Result.Value = list;
                if (list == null || !list.Any())
                {
                    Result.AddMessage("No Product categories available");
                }
            }
            catch (Exception)
            {
                Result.AddMessage("No Product categories available");
            }
            return Result;
        }   
    }
}
