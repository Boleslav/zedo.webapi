﻿using System;
using System.Linq;
using BusinessLayer;

namespace ZedoWebApi.Controllers
{
    public class MallsController : BaseController
    {
        private readonly MallsViewRepository _repository;
        public MallsController()
        {
            _repository = new MallsViewRepository();
        }

        public object GetAll()
        {
            try
            {
                var list = _repository.GetAll();
                Result.Value = list;
                if (list == null || !list.Any())
                {
                    Result.AddMessage("No malls to display");
                }
            }
            catch (Exception)
            {
                Result.AddMessage("No malls to display");
            }
            return Result;
        }
    }
}
