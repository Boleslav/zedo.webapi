﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web.Http;
using BusinessLayer.Common;
using Common;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace ZedoWebApi.Controllers
{
    public class BaseController : ApiController
    {
        protected string Message = "";
        protected GenericResult Result;
        protected const string SaveError = "An error occurred while saving. Try again later";
        protected const string DeleteError = "An error occurred while deleting. Try again later";
        public BaseController()
        {
            Result = new GenericResult();
        }

        protected bool ValidateModel()
        {
            if (!ModelState.IsValid)
            {
                foreach (var modelStateItem in ModelState.Values)
                {
                    Result.AddMessages(modelStateItem.Errors.Select(modelErrorItem => modelErrorItem.ErrorMessage));
                }
            }
            return ModelState.IsValid;
        }

        protected bool SendSms(string mobileNo, string code)
        {
            try
            {
                string accountSid = ConfigurationManager.AppSettings["AccountSid"];
                string authToken = ConfigurationManager.AppSettings["AuthToken"];
                TwilioClient.Init(accountSid, authToken);

                var message = MessageResource.Create(
                    to: new PhoneNumber("+974" + mobileNo),
                    from: new PhoneNumber(ConfigurationManager.AppSettings["FromNumber"]),
                    body: $"Your Zedo activation code is {code}");
                return !string.IsNullOrWhiteSpace(message.Sid);
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected string RandomString(int length)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var numbers = "0123456789";
            var stringChars = new char[length];
            var random = new Random();

            for (int i = 0; i < 1; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }
            stringChars[1] = '-';
            for (int i = 2; i < length; i++)
            {
                stringChars[i] = numbers[random.Next(numbers.Length)];
            }

            var finalString = new String(stringChars);
            return finalString;
        }

        protected bool ActivateAccount(string email, string activationToken, string name = "Customer")
        {
            string siteUrl = ConfigurationManager.AppSettings.Get("SiteUrl");
            var bodyBuilder = new StringBuilder();
            bodyBuilder.AppendLine("<table>");
            bodyBuilder.AppendLine("<tbody style='font-size: 15px'>");
            bodyBuilder.AppendLine("<tr><td style='padding-bottom:30px;'><h2>Dear " + name + ",</h2></td></tr>");
            bodyBuilder.AppendLine("<tr><td style='padding-bottom:15px;'>Thank you for registering to Zedo Qatar.</td></tr>");
            bodyBuilder.AppendLine("<tr><td style='padding-bottom:15px;'>To activate your account, please click on the link below:</td></tr>");
            bodyBuilder.AppendFormat("<tr><td><a href='{0}' target='_blank'>Activate account</a></td></tr>", siteUrl + "/Customer/Activate/" + activationToken);
            bodyBuilder.AppendLine("<tr><td style='padding:15px 27px 0 0;'>Thank you,</td></tr>");
            bodyBuilder.AppendLine("<tr><td style='padding:0 27px 30px 0;'>Zedo Support Team</td></tr>");
            bodyBuilder.AppendLine("</tbody>");
            bodyBuilder.AppendLine("</table>");

            return SendMail(email, "Zedo - Activate Account", bodyBuilder.ToString());
        }

        protected bool RecoverAccount(string fullName, string email, string password)
        {
            var bodyBuilder = new StringBuilder();
            bodyBuilder.AppendLine("<table>");
            bodyBuilder.AppendLine("<tbody style='font-size: 15px'>");
            bodyBuilder.AppendFormat("<tr><td style='padding-bottom:15px;'>Dear {0};</td></tr>", fullName);
            bodyBuilder.AppendLine("<tr><td style='padding-bottom:15px;'>You have requested that you have forgotten your password. Your account info is listed below.</td></tr>");
            bodyBuilder.AppendFormat("<tr><td><span style='font-weight:bold;'>Email : </span>{0}</td></tr>", email);
            bodyBuilder.AppendFormat("<tr><td><span style='font-weight:bold;'>Password : </span>{0}</td></tr>", password);
            bodyBuilder.AppendLine("<tr><td style='padding:15px 27px 0 0;'>Thank you,</td></tr>");
            bodyBuilder.AppendLine("<tr><td style='padding:0 27px 30px 0;'>Zedo Support Team</td></tr>");
            bodyBuilder.AppendLine("</tbody>");
            bodyBuilder.AppendLine("</table>");

            return SendMail(email, "Zedo - Account Recovery", bodyBuilder.ToString());
        }

        protected bool SendMail(string mailAddressTo, string subject, string body)
        {
            var smtp = new SmtpClient
            {
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(ConfigurationManager.AppSettings["MailFrom"], ConfigurationManager.AppSettings["MailPassword"]),
                Host = ConfigurationManager.AppSettings["Host"],
                Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"])
            };
            var mailResult = MailHelper.SendMail(new MailAddress(ConfigurationManager.AppSettings["MailFrom"], "Zedo"), new MailAddress(mailAddressTo), subject, body, true, MailPriority.Normal, smtp);
            return mailResult.result;
        }
    }
}
