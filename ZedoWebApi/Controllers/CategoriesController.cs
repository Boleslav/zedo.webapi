﻿using System;
using System.Configuration;
using System.Net;
using System.Web.Http;
using BusinessLayer;
using BusinessLayer.Common;

namespace ZedoWebApi.Controllers
{
    public class CategoriesController : BaseController
    {
        readonly ProductCategoriesRepository _repository;
        public CategoriesController()
        {
            _repository = new ProductCategoriesRepository();
        }

        public object GetAll()
        {
            var result = new GenericResult();
            try
            {
                var list = _repository.GetAll();
                list.ForEach(e => e.Image = e.Image?.Replace("~", SiteUrl));
                result.Value = list;
            }
            catch (Exception ex)
            {
                result.AddMessage(ex.Message);
                result.Value = null;
            }
            return result;
        }

        public object Get(int id)
        {
            var result = new GenericResult();
            try
            {
                var entity = _repository.GetById(id);
                if (entity != null)
                {
                    entity.Image = entity.Image?.Replace("~", SiteUrl);
                    result.Value = entity;
                }
                else
                {
                    result.AddMessage("Category not found");
                }
            }
            catch (Exception ex)
            {
                result.AddMessage(ex.Message);
                result.Value = null;
            }
            return result;
        }

        [HttpGet]
        [Route("~/api/ProductCategories/Search/{id}")]
        public object Search(string id)
        {
            var result = new GenericResult();
            try
            {
                var list = _repository.FindAll(e => e.NameEn.Contains(id) || e.NameAr.Contains(id));
                if (list != null)
                {
                    list.ForEach(e => e.Image = e.Image?.Replace("~", SiteUrl));
                    result.Value = list;
                }
                else
                {
                    result.AddMessage("Category not found");
                }
            }
            catch (Exception ex)
            {
                result.AddMessage(ex.Message);
                result.Value = null;
            }
            return result;
        }
    }
}
