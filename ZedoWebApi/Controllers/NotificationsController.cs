﻿using System;
using System.Linq;
using BusinessLayer;

namespace ZedoWebApi.Controllers
{
    public class NotificationsController : BaseController
    {
        private readonly NotificationsRepository _repository;
        public NotificationsController()
        {
            _repository = new NotificationsRepository();
        }

        public object GetAll()
        {
            try
            {
                var list = _repository.FindAll(e => !e.IsRemoved);
                Result.Value = list;
                if (list == null || !list.Any())
                {
                    Result.AddMessage("No notifications available");
                }
            }
            catch (Exception)
            {
                Result.AddMessage("No notifications available");
            }
            return Result;
        }
    }
}
