﻿using System;
using System.Linq;
using System.Web.Http;
using BusinessLayer;


namespace ZedoWebApi.Controllers
{
    public class FavoritesController : BaseController
    {
        private readonly CustomerFavoriteStoresRepository _repository;
        public FavoritesController()
        {
            _repository = new CustomerFavoriteStoresRepository();
        }

        [Route("~/api/Favorites/Customer/{customerId}")]
        public object GetAllByCustomer(long customerId)
        {
            try
            {
                var list = new StoresViewRepository().GetAllByCustomerFavorites(customerId);
                Result.Value = list;
                if (list == null || !list.Any())
                {
                    Result.AddMessage("No favorites to display");
                }
            }
            catch (Exception)
            {
                Result.AddMessage("No favorites to display");
            }
            return Result;
        }

        public class CustomerFavoriteStoreRequest
        {
            public long CustomerId { get; set; }
            public int StoreId { get; set; }
        }
        [HttpPost]
        public object Post([FromBody] CustomerFavoriteStoreRequest data)
        {
            try
            {
                if (_repository.GetCount(e => e.FKCustomerID == data.CustomerId && e.FKStoreID == data.StoreId) > 0)
                {
                    Result.AddMessage("Store already added to favorites");
                }

                if (Result.Status)
                {
                    var result = _repository.Add(new CustomerFavoriteStores { FKCustomerID = data.CustomerId, FKStoreID = data.StoreId });
                    Result.Value = result.Value;
                    if (!result.Status)
                    {
                        Result.AddMessage(SaveError);
                    }
                }
            }
            catch (Exception)
            {
                Result.AddMessage(SaveError);
            }
            return Result;
        }

        [HttpDelete]
        public object Delete([FromBody] CustomerFavoriteStoreRequest data)
        {
            try
            {
                bool isDeleted = _repository.RemoveByCustomerStore(data.CustomerId, data.StoreId);
                Result.Value = isDeleted;
                if (!isDeleted)
                {
                    Result.AddMessage(DeleteError);
                }
            }
            catch (Exception)
            {
                Result.AddMessage(DeleteError);
            }
            return Result;
        }
    }
}
