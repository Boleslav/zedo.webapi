﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using BusinessLayer;

namespace ZedoWebApi.Controllers
{
    public class StoresController : BaseController
    {
        private readonly StoresViewRepository _repository;
        public StoresController()
        {
            _repository = new StoresViewRepository();
        }

        public object GetAll()
        {
            try
            {
                var list = _repository.GetAll();
                Result.Value = list;
                if (list == null || !list.Any())
                {
                    Result.AddMessage("No stores to display");
                }
            }
            catch (Exception)
            {
                Result.AddMessage("No stores to display");
            }
            return Result;
        }

        public object Get(int id)
        {
            try
            {
                var entity = _repository.GetWithDetails(id);
                Result.Value = entity;
                if (entity == null || entity.StoreID == 0)
                {
                    Result.AddMessage("No store found");
                }
            }
            catch (Exception)
            {
                Result.AddMessage("No store found");
            }
            return Result;
        }

        [Route("~/api/Stores/Search/")]
        public object Get([FromUri]StoresSearch search)
        {
            try
            {
                var list = _repository.Search(search ?? new StoresSearch());
                Result.Value = list;
                if (list == null || !list.Any())
                {
                    Result.AddMessage("No results to display");
                }
            }
            catch (Exception)
            {
                Result.AddMessage("No results to display");
            }
            return Result;
        }

        [Route("~/api/Stores/Customer/{storeId}/{customerId}")]
        public object GetByCustomer(int storeId, long customerId)
        {
            try
            {
                var entity = _repository.GetWithDetails(storeId, customerId);
                Result.Value = entity;
                if (entity == null || entity.StoreID == 0)
                {
                    Result.AddMessage("No store found");
                }
            }
            catch (Exception)
            {
                Result.AddMessage("No store found");
            }
            return Result;
        }

        [Route("~/api/Stores/Mall")]
        public object GetAllByMall([FromUri]int[] id)
        {
            try
            {
                var list = _repository.GetAllByMallIds(id);
                Result.Value = list;
                if (list == null || !list.Any())
                {
                    Result.AddMessage("No stores to display");
                }
            }
            catch (Exception)
            {
                Result.AddMessage("No stores to display");
            }
            return Result;
        }

        [Route("~/api/Stores/Brand")]
        public object GetAllByBrand([FromUri]int[] id)
        {
            try
            {
                var list = _repository.GetAllByBrandIds(id);
                Result.Value = list;
                if (list == null || !list.Any())
                {
                    Result.AddMessage("No stores to display");
                }
            }
            catch (Exception)
            {
                Result.AddMessage("No stores to display");
            }
            return Result;
        }

        [Route("~/api/Stores/Location")]
        public object GetAllByLocation([FromUri] List<int> id)
        {
            try
            {
                var list = _repository.FindAll(e => id.Contains(e.LocationID ?? 0));
                Result.Value = list;
                if (list == null || !list.Any())
                {
                    Result.AddMessage("No stores to display");
                }
            }
            catch (Exception)
            {
                Result.AddMessage("No stores to display");
            }
            return Result;
        }

        [Route("~/api/Stores/ProductCategory")]
        public object GetAllByProductCategory([FromUri] int[] id)
        {
            try
            {
                var list = _repository.GetAllByProductCategoryIds(id);
                Result.Value = list;
                if (list == null || !list.Any())
                {
                    Result.AddMessage("No stores to display");
                }
            }
            catch (Exception)
            {
                Result.AddMessage("No stores to display");
            }
            return Result;
        }
    }
}
