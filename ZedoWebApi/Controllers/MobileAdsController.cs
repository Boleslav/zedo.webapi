﻿using System;
using BusinessLayer;

namespace ZedoWebApi.Controllers
{
    public class MobileAdsController : BaseController
    {
        private readonly MobileAdsViewRepository _repository;
        public MobileAdsController()
        {
            _repository = new MobileAdsViewRepository();
        }

        public object Get(byte id = 0)
        {
            /*
             Home = 1,
             Malls = 2,
             Stores = 3,
             Locations = 4,
             Brands = 5
            */
            try
            {
                var entities = id > 0
                    ? new[] {_repository.GetByMobilePage((MobilePages) id)}
                    : _repository.GetAll().ToArray();

                Result.Value = entities;
            }
            catch (Exception)
            {
                Result.AddMessage("No ad found");
            }
            return Result;
        }
    }
}
