﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using BusinessLayer;
using BusinessLayer.Common;


namespace ZedoWebApi.Controllers
{
    public class CustomersController : BaseController
    {
        readonly CustomersRepository _repository;
        public CustomersController()
        {
            _repository = new CustomersRepository();
        }

        public object Get(long id)
        {
            try
            {
                var entity = _repository.Find(e => e.CustomerID == id && !e.IsRemoved);
                Result.Value = entity;
                if (entity == null || entity.CustomerID == 0)
                {
                    Result.AddMessage("No customer found");
                }
            }
            catch (Exception)
            {
                Result.AddMessage("No customer found");
            }
            return Result;
        }

        [HttpGet]
        [Route("~/api/Customers/Login/{email}/{password}")]
        public object Login(string email, string password)
        {
            try
            {
                var entity = _repository.Find(e => e.Email == email && e.Password == password && !e.IsRemoved);
                Result.Value = entity;
                if (entity != null)
                {
                    if (!entity.IsActive || !entity.IsPhoneActive)
                    {
                        Result.AddMessage("Account not activated");
                    }
                }
                else
                {
                    Result.AddMessage("Invalid email or password");
                }
            }
            catch (Exception)
            {
                Result.AddMessage("Invalid email or password");
            }
            return Result;
        }

        [HttpGet]
        [Route("~/api/Customers/Recover/{email}")]
        public object Recover(string email)
        {
            try
            {
                var entity = _repository.Find(e => e.Email == email && !e.IsRemoved);
                Result.Value = entity;
                if (entity != null)
                {
                    if (!entity.IsActive || !entity.IsPhoneActive)
                    {
                        Result.AddMessage("Account not activated");
                    }
                    else
                    {
                        Task.Factory.StartNew(() =>
                        {
                            RecoverAccount(entity.CustomerName, entity.Email, entity.Password);
                        });
                    }
                }
                else
                {
                    Result.AddMessage("Email address does not exist");
                }
            }
            catch (Exception)
            {
                Result.AddMessage("Email address does not exist");
            }
            return Result;
        }

        [Route("~/api/Customers/PinCode/{id}/{pinCode}")]
        public object GetPinCode(int id, string pinCode)
        {
            try
            {
                var entity = _repository.Find(c => c.CustomerID == id && c.Pin.Equals(pinCode));
                Result.Value = entity;
                if (entity == null)
                {
                    Result.AddMessage("Invalid pin code");
                }
            }
            catch (Exception)
            {
                Result.AddMessage("Invalid pin code");
            }
            return Result;
        }

        [HttpPut]
        public object ChangePin(int id, string pinCode)
        {
            var result = new GenericResult();
            try
            {
                var entity = _repository.GetById(id);
                if (entity != null)
                {
                    entity.Pin = pinCode;
//                    entity.ConfirmPassword = entity.Password;
                    result = _repository.Update(entity);
                    if (!result.Status)
                    {
                        result.AddMessage("Saving Error");
                    }
                }
                else { result.AddMessage("User not found"); }
            }
            catch (Exception ex)
            {
                result.AddMessage(ex.Message);
                result.Value = null;
            }
            return result;
        }

        [HttpPut]
        [Route("~/api/Customers/ActivatePhone/{id}")]
        public object ActivatePhone(int id, [FromBody]string code)
        {
            try
            {
                var entity = _repository.Find(c => c.PhoneCode.Equals(code) && c.CustomerID == id);
                if (entity != null)
                {
                    entity.IsActive = true;
                    entity.IsPhoneActive = true;
//                    entity.ConfirmPassword = entity.Password;
                    var result = _repository.Update(entity);
                    Result.Value = entity;
                    if (!result.Status)
                    {
                        Result.AddMessage("Activation Error");
                    }
                }
                else
                {
                    Result.AddMessage("Customer not found");
                }
            }
            catch (Exception)
            {
                Result.AddMessage(SaveError);
            }
            return Result;
        }

        [HttpPut]
        [Route("~/api/Customers/ResendPhoneCode/{id}")]
        public object ResendPhoneCode(int id)
        {
            try
            {
                var entity = _repository.GetById(id);
                if (entity != null)
                {
                    entity.PhoneCode = RandomString(6);
//                    entity.ConfirmPassword = entity.Password;
                    var result = _repository.Update(entity);
                    Result.Value = entity;
                    if (result.Status)
                    {
                        SendSms(entity.Phone, entity.PhoneCode);
                    }
                    else
                    {
                        Result.AddMessage(SaveError);
                    }
                }
                else
                {
                    Result.AddMessage("Customer not found");
                }
            }
            catch (Exception)
            {
                Result.AddMessage(SaveError);
            }
            return Result;
        }

        [HttpPost]
        public object Register([FromBody]Customers entity)
        {
            try
            {
                if (ValidateModel())
                {
                    if (_repository.GetCount(e => e.Phone.Equals(entity.Phone)) > 0)
                    {
                        Result.AddMessage("Mobile already exists");
                    }

                    if (_repository.GetCount(e => e.Email.Equals(entity.Email)) > 0)
                    {
                        Result.AddMessage("Email already exists");
                    }

                    if (Result.Status)
                    {
                        entity.IsActive = false;
                        entity.IsPhoneActive = false;
                        entity.ActivationCode = Guid.NewGuid().ToString("N");
                        entity.PhoneCode = RandomString(6);
                        entity.Pin = "0000";
                        entity.IsRemoved = false;
                        var result = _repository.Add(entity);
                        Result.Value = entity;
                        if (result.Status)
                        {
                            SendSms(entity.Phone, entity.PhoneCode);
                        }
                        else
                        {
                            Result.AddMessage(SaveError);
                        }
                    }
                }
            }
            catch (Exception)
            {
                Result.AddMessage(SaveError);
            }
            return Result;
        }

        [HttpPut]
        public object Save([FromBody]Customers entity)
        {
            try
            {
                if (ValidateModel())
                {
                    var existingUser = _repository.Find(x => /*x.Phone == entity.Phone && */x.Email == entity.Email);
                    if (existingUser == null)
                    {
                        Result.AddMessage("User email not found");
                        Result.Value = null;
                    }
                    else
                    {
                        existingUser.IsNewsSubscriber = entity.IsNewsSubscriber;
                        existingUser.Countries = entity.Countries;
                        existingUser.Gender = entity.Gender;
                        existingUser.Phone = entity.Phone;
                        existingUser.CustomerName = entity.CustomerName;

                        _repository.Update(existingUser);

                        // todo: consider hiding particular fields in existingUser object and return it
                        Result.Value = entity;
                    }
                }
            }
            catch (Exception)
            {
                Result.AddMessage(SaveError);
            }
            return Result;
        }
    }
}
