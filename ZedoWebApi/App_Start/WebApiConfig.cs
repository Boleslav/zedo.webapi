﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace ZedoWebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        { // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Formatters.Remove(config.Formatters.XmlFormatter);//allow api to return json formatter only by removing xml formatter.

            //Allow cross-origin for every domain to access the api
            //var cors = new EnableCorsAttribute("*", "*", "*");
            // config.EnableCors(cors);

            // config.Filters.Add(new RequireHttpsAttribute());// allow https
            //config.Filters.Add(new BasicAuthenticationAttribute());// allow Authentication
        }
    }
}
