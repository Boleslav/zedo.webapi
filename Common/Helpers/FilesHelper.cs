﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;

namespace Common
{
    /// <summary>
    /// Files Helper Contain Common Methods Dealing With Files V 1.1
    /// </summary>
    public class FilesHelper
    {
        /// <summary>
        /// Enum For File Size Uint
        /// </summary>
        public enum SizeUnit { InBytes, InKiloBytes, InMegaBytes };

        /// <summary>
        /// Enum File Size Check Critera
        /// </summary>
        public enum FileSizeCheck { SmallerThan, EqualTo, BiggerThan };

        /// <summary>
        /// Check if is valid content
        /// </summary>
        public enum IsValidContent { Valid, NotValid };

        /// <summary>
        /// Enum For Image Size
        /// </summary>
        public enum ImageSize
        {
            Small, Medium, Large, True, Custom, specificSize
        }
        public enum UploadStatus { Empty, Success, Error }
        public struct UploadResult
        {
            private string _oldFileName;
            public UploadStatus Status { get; set; }
            public string Message { get; set; }
            public string UploadedFileName { get; set; }
            public string UploadPath { get; set; }
            public string OldFileName
            {
                get { return !string.IsNullOrEmpty(_oldFileName) ? VirtualPathUtility.GetFileName(_oldFileName) : null; }
                set { _oldFileName = value; }
            }

            /// <summary>
            /// Delete old file after successfully saving to database
            /// </summary>
            public void Commit()
            {
                if (Status == UploadStatus.Success && !string.IsNullOrEmpty(OldFileName))
                {
                    DeleteFile(OldFileName, UploadPath);
                }
            }

            /// <summary>
            /// Delete the new uploaded file if an error occurred
            /// </summary>
            public void Rollback()
            {
                if (Status == UploadStatus.Success)
                {
                    DeleteFile(UploadedFileName, UploadPath);
                }
            }
        }
        /// <summary>
        /// Convert File to Array of Bytes
        /// </summary>
        /// <param name="FileFullPath">The full path of the file on the host server
        /// Ex : http://Websites/Folder/filename.ext 
        /// </param>
        /// <returns></returns>
        public static byte[] ConvertToBytes(string FileFullPath)
        {
            try
            {
                byte[] fileBytes = new System.Net.WebClient().DownloadData(FileFullPath);
                return fileBytes;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Convert File to Array of Bytes
        /// </summary>
        /// <param name="FileFullPath">The full path of the file on the host server
        /// Ex : ~/Folder/filename.ext 
        /// </param>
        /// <returns></returns>
        public static byte[] GetFileBytes(string fileFullPath)
        {
            try
            {
                return File.ReadAllBytes(HttpContext.Current.Server.MapPath(fileFullPath));
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Convert File to Array of Bytes
        /// </summary>
        /// <param name="FileFullPath">The full path of the file on the host server
        /// Ex : ~/Folder/filename.ext 
        /// </param>
        /// <param name="customFileName">The new file name without extension
        /// Ex : MyNewFile
        /// </param>
        /// <returns></returns>
        public static byte[] GetFileBytes(string fileFullPath, ref string customFileName)
        {
            try
            {
                string physicalPath = HttpContext.Current.Server.MapPath(fileFullPath);
                string fileExt = Path.GetExtension(physicalPath).ToLower();
                customFileName += fileExt;
                return File.ReadAllBytes(physicalPath);
            }
            catch
            {
                customFileName = string.Empty;
                return null;
            }
        }

        /// <summary>
        /// Save Byte[] as file in specific path
        /// </summary>
        /// <param name="fileContent">File Content Byte[]</param>
        /// /// <param name="fileName">Old File Name</param>
        /// <param name="Path">save path in the server</param>
        /// <param name="uploadedFileName">new file name</param>
        /// <returns>return if save done or no</returns>
        public static bool SaveFile(byte[] fileContent, string fileName, string Path, out string uploadedFileName)
        {
            try
            {
                string fileExtension = fileName.Substring(fileName.LastIndexOf("."));
                string OutFileName = Guid.NewGuid().ToString().Replace("-", "") + fileExtension;
                string filePath = HttpContext.Current.Server.MapPath(HttpContext.Current.Request.ApplicationPath + "//" + Path + "//" + OutFileName);
                File.WriteAllBytes(filePath, fileContent);
                uploadedFileName = OutFileName;
                return true;
            }
            catch (IOException iox)
            {
                uploadedFileName = iox.Message;
                return false;
            }
        }

        /// <summary>
        /// Upload file to specific folder 
        /// </summary>
        /// <param name="UploadControl">File Upload control that contains the file path in the client pc</param>
        /// <param name="Path">Upload path in the server</param>
        /// <returns>new file name</returns>
        public static bool UploadFile(HttpPostedFileWrapper file, string path, out string uploadedFileName)
        {
            try
            {
                string fileExtension = System.IO.Path.GetExtension(file.FileName);
                string outFileName = Guid.NewGuid().ToString().Replace("-", "") + fileExtension;
                file.SaveAs(GetFileFullPath(path, outFileName));
                uploadedFileName = outFileName;
                return true;
            }
            catch (IOException iox)
            {
                uploadedFileName = iox.Message;
                return false;
            }
        }

        /// <summary>
        /// Upload file to specific folder 
        /// </summary>
        /// <param name="UploadControl">File Upload control that contains the file path in the client pc</param>
        /// <param name="Path">Upload path in the server</param>
        /// <returns>new file name</returns>
        public static bool UploadFile(HttpPostedFileBase file, string path, out string uploadedFileName)
        {
            try
            {
                string fileExtension = System.IO.Path.GetExtension(file.FileName);
                string outFileName = Guid.NewGuid().ToString().Replace("-", "") + fileExtension;
                file.SaveAs(GetFileFullPath(path, outFileName));
                uploadedFileName = outFileName;
                return true;
            }
            catch (IOException iox)
            {
                uploadedFileName = iox.Message;
                return false;
            }
        }

        /// <summary>
        /// Upload file to specific folder 
        /// </summary>
        /// <param name="UploadControl">File Upload control that contains the file path in the client pc</param>
        /// <param name="Path">Upload path in the server</param>
        /// <returns>new file name</returns>
        public static bool UploadFile(HttpPostedFileWrapper file, string Path, string customName, out string uploadedFileName)
        {
            try
            {
                string fileExtension = System.IO.Path.GetExtension(file.FileName);
                string OutFileName = customName + fileExtension;
                file.SaveAs(GetFileFullPath(Path, OutFileName));
                uploadedFileName = OutFileName;
                return true;
            }
            catch (IOException iox)
            {
                uploadedFileName = iox.Message;
                return false;
            }
        }

        /// <summary>
        /// Get The Uploaded Files Byte To Save IT to DB
        /// Return true or false depend on file reading file bytes
        /// </summary>
        /// <param name="file">Uploaded Files</param>
        /// <param name="uploadedFile">out param return the file bytes</param>
        /// <returns>Return true or false depend on file reading file bytes</returns>
        public static bool GetUploadedFileByte(HttpPostedFileWrapper file, out byte[] uploadedFile)
        {
            uploadedFile = new byte[] { };
            try
            {
                if (file != null && file.ContentLength > 0)
                {
                    Int32 length = file.ContentLength;
                    byte[] tempFile = new byte[length];
                    file.InputStream.Read(tempFile, 0, length);
                    uploadedFile = tempFile;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete File From Server 
        /// </summary>
        /// <param name="fileFullPath">The virtual path of the file on the server (e.g. ~/uploads/filename.ext)</param>
        public static bool DeleteFile(string fileVirtualPath)
        {
            try
            {
                var fileToDelete = new FileInfo(HttpContext.Current.Server.MapPath(fileVirtualPath));
                if (!fileToDelete.Exists)
                {
                    return false;
                }
                fileToDelete.Delete();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete File Form Server Used To Delete Pre Uploaded 
        /// </summary>
        /// <param name="fileName">File Name We Want To Delete</param>
        /// <param name="filePath">The File Path In The Sever</param>
        public static bool DeleteFile(string fileName, string filePath)
        {
            try
            {
                File.Delete(GetFileFullPath(filePath, fileName));
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Update file used to delete file from server and replace it with another file
        /// </summary>
        /// <param name="UploadControl">File Upload control that contains the file path in the client pc</param>
        /// <param name="Path">Upload/Delete path in the server</param>
        /// <param name="oldFileName">File Name We Want To Delete</param>
        /// <returns>new file name</returns>
        public static bool UpdateFile(HttpPostedFileWrapper file, string Path, string oldFileName, out string uploadedFileName)
        {
            try
            {
                string outfilename = string.Empty;
                DeleteFile(oldFileName, Path);
                if (UploadFile(file, Path, out outfilename))
                {
                    uploadedFileName = outfilename;
                    return true;
                }
                else
                {
                    uploadedFileName = string.Empty;
                    return false;
                }
            }
            catch (System.IO.IOException iox)
            {
                uploadedFileName = iox.Message;
                return false;
            }
        }

        /// <summary>
        /// Extract File Name From Full File Path
        /// </summary>
        /// <param name="filePath">File Path or Virtual Path</param>
        /// <returns></returns>
        public static string ExtractFileName(string filePath)
        {
            return System.IO.Path.GetFileName(filePath);
            //return filePath.Substring(filePath.LastIndexOf("/") + 1);
        }

        /// <summary>
        /// Compare the size of the file to a given size
        /// </summary>
        /// <param name="fileBytes">The file bytes</param>
        /// <param name="sizeToBeComparedTo">The size to be compared to</param>
        /// <param name="sizeUnit">The Unit of the size to be compared to</param>
        /// <returns></returns>
        public static FileSizeCheck CheckFileSize(byte[] fileBytes, int sizeToBeComparedTo, SizeUnit sizeUnit)
        {
            FileSizeCheck res = FileSizeCheck.EqualTo;

            switch (sizeUnit)
            {
                case SizeUnit.InBytes:
                    {
                        if (fileBytes.Length < sizeToBeComparedTo)
                        {
                            res = FileSizeCheck.SmallerThan;
                        }
                        else if (fileBytes.Length == sizeToBeComparedTo)
                        {
                            res = FileSizeCheck.EqualTo;
                        }
                        else
                        {
                            res = FileSizeCheck.BiggerThan;
                        }
                    }

                    break;
                case SizeUnit.InKiloBytes:
                    {
                        int fileSizeInKB = fileBytes.Length / 1024;
                        if (fileSizeInKB < sizeToBeComparedTo)
                        {
                            res = FileSizeCheck.SmallerThan;
                        }
                        else if (fileSizeInKB == sizeToBeComparedTo)
                        {
                            res = FileSizeCheck.EqualTo;
                        }
                        else
                        {
                            res = FileSizeCheck.BiggerThan;
                        }
                    }

                    break;
                case SizeUnit.InMegaBytes:
                    {
                        int fileSizeInKB = fileBytes.Length / 1024;
                        int fileSizeInMB = fileSizeInKB / 1024;

                        if (fileSizeInMB < sizeToBeComparedTo)
                        {
                            res = FileSizeCheck.SmallerThan;
                        }
                        else if (fileSizeInMB == sizeToBeComparedTo)
                        {
                            res = FileSizeCheck.EqualTo;
                        }
                        else
                        {
                            res = FileSizeCheck.BiggerThan;
                        }

                    }
                    break;
            }
            return res;
        }

        /// <summary>
        /// Check a file type to a range of file types values
        /// </summary>
        /// <param name="checkFileName">The file name to be checked</param>
        /// <param name="fileTypeConfigKey">The key in the config file that contains the range of file types</param>
        /// <returns>true if the file type is valid</returns>
        public static bool CheckFileTypes(string checkFileName, string fileTypeConfigKey)
        {
            bool IsValid = false;
            string fileExt = Path.GetExtension(checkFileName).TrimStart('.');
            string[] sep = new string[2];
            sep[0] = ".";
            sep[1] = ",";
            string[] AllowedTypes = System.Configuration.ConfigurationManager.AppSettings[fileTypeConfigKey].Split(sep, StringSplitOptions.RemoveEmptyEntries);
            foreach (string type in AllowedTypes)
            {
                if (type.ToLower() == fileExt.ToLower())
                {
                    IsValid = true;
                    break;
                }
            }

            return IsValid;
        }

        /// <summary>
        /// Check Image Width & Height compared to given values
        /// </summary>
        /// <param name="picturePath">Picture Path on the server not client pc</param>
        /// <param name="maxWidth">Maximum Width For Image</param>
        /// <param name="maxHeight">Maximum Height For Image</param>
        /// <returns>IsValidContent For the Image</returns>
        public static IsValidContent CheckImageWidthHeight(string imagePath, int maxWidth, int maxHeight)
        {
            int imgHeight = System.Drawing.Image.FromFile(imagePath).Height;
            int imgWidth = System.Drawing.Image.FromFile(imagePath).Width;
            if (imgHeight <= maxHeight && imgWidth <= maxWidth)
            {
                return IsValidContent.Valid;
            }
            else
            {
                return IsValidContent.NotValid;
            }
        }

        /// <summary>
        /// <para>Get The Current Pages</para>
        /// <para>Ex : Default.aspx</para>  
        /// <para>Return : String Page Name</para>  
        /// </summary>
        /// <returns>String Page Name</returns>
        public static string GetCurrentPageName()
        {
            string sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
            System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
            string sRet = oInfo.Name;
            return sRet;
        }

        /// <summary>
        /// Genertate Image With GDI+ Using the defined width and height in Image Size Enumerator
        /// </summary>
        /// <param name="ImagePath">Path of image you wont to Create thumbnail of it </param>
        /// <param name="Size">Thumbnail size </param>
        /// <returns></returns>
        private static byte[] CreateThumbnailImage(string ImagePath, ImageSize Size)
        {
            byte[] Retuned = new byte[] { };

            System.Drawing.Image oldOne = null;
            if (ImagePath.StartsWith("/"))
            {
                oldOne = System.Drawing.Image.FromFile(ImagePath);
            }
            else
            {
                WebClient wc = new WebClient();
                try
                {

                    oldOne = System.Drawing.Image.FromStream(wc.OpenRead(HttpUtility.HtmlEncode(ImagePath)));
                }
                catch (System.Exception)
                {

                }

            }
            if (oldOne != null)
            {

                Size s = new Size();
                switch (Size)
                {
                    /*600px*/
                    case ImageSize.Large:
                        s = CalculateDimensions(oldOne.Size, 600);
                        break;
                    /*400px*/
                    case ImageSize.Medium:
                        s = CalculateDimensions(oldOne.Size, 400);
                        break;
                    /*100px*/
                    case ImageSize.Small:
                        s = CalculateDimensions(oldOne.Size, 110);
                        break;
                    case ImageSize.Custom:
                        s = CalculateDimensions(oldOne.Size, 90);
                        break;
                    case ImageSize.True:
                        s = CalculateDimensions(oldOne.Size, oldOne.Size.Width);
                        break;
                    case ImageSize.specificSize:
                        s.Width = 92;
                        s.Height = 58;
                        break;

                }

                Bitmap NewImage = new Bitmap(oldOne, s);

                Graphics g = Graphics.FromImage(NewImage);
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                g.DrawImage(oldOne, new Rectangle(new Point(0, 0), s));

                System.IO.MemoryStream mStream = new MemoryStream();
                NewImage.Save(mStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                Retuned = mStream.GetBuffer();

                mStream.Dispose();
                oldOne.Dispose();
                NewImage.Dispose();
                g.Dispose();

            }
            return Retuned;
        }

        /// <summary>
        /// Calculate Image Dimensions used with CreateThumbnailImage() Method
        /// </summary>
        /// <param name="oldSize">image old size</param>
        /// <param name="targetSize">image new size</param>
        /// <returns>image size</returns>
        private static Size CalculateDimensions(Size oldSize, int targetSize)
        {
            Size newSize = new Size();
            if (oldSize.Height > oldSize.Width)
            {
                newSize.Width = (int)(oldSize.Width * ((float)targetSize / (float)oldSize.Height));
                newSize.Height = targetSize;
            }
            else
            {
                newSize.Width = targetSize;
                newSize.Height = (int)(oldSize.Height * ((float)targetSize / (float)oldSize.Width));
            }
            return newSize;
        }

        /// <summary>
        /// <para>Create Physical Thubmnail Image Form Normal Image</para>
        /// <para>Note* the file name will be the same for both of them  </para>
        /// <para>except you add extension</para>
        /// </summary>
        /// <param name="imageName">Original Image Name <para>ex : 'File.jpg'</para></param>
        /// <param name="imagePath"><para>Original Image Path</para><para>ex : 'Uploads/Images' </para></param>
        /// <param name="width">Thumbnail Width</param>
        /// <param name="height">Thumbnail Height</param>
        /// <param name="thumbPath">Thumbnail Path</param>
        /// <param name="extension"><para>Any text inserted after Thumb Name <para>ex :'File_thumb.jpg'</para></param>
        /// <returns></returns>
        public static bool CreateImageThumb(string imageName, string imagePath, int width, int height, string thumbPath, string extension)
        {
            try
            {
                string oldFilePath = HttpContext.Current.Server.MapPath(HttpContext.Current.Request.ApplicationPath + "//" + imagePath + "//" + imageName);
                string NewFilePath;
                if (!String.IsNullOrEmpty(extension))
                {
                    string fileExtension = imageName.Substring(imageName.LastIndexOf("."));
                    NewFilePath = HttpContext.Current.Server.MapPath(HttpContext.Current.Request.ApplicationPath + "//" + thumbPath + "//" + imageName.Replace(fileExtension, extension + fileExtension));
                }
                else
                {
                    NewFilePath = HttpContext.Current.Server.MapPath(HttpContext.Current.Request.ApplicationPath + "//" + thumbPath + "//" + imageName);
                }
                System.Drawing.Image image = System.Drawing.Image.FromFile(oldFilePath);
                Bitmap bmp = new Bitmap(width, height);
                System.Drawing.Graphics gr = System.Drawing.Graphics.FromImage(bmp);
                gr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                gr.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                gr.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
                System.Drawing.Rectangle rectDestination = new System.Drawing.Rectangle(0, 0, width, height);
                gr.DrawImage(image, rectDestination, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel);
                bmp.Save(NewFilePath);
                bmp.Dispose();
                image.Dispose();
                return true;
            }
            catch
            {
                return false;
            }

        }

        /// <summary>
        /// Creates the specified sub-directory, if it doesn't exist.
        /// </summary>
        /// <param name="name">The name of the sub-directory to be created.</param>
        internal static void CreateSubDirectory(string name)
        {
            if (Directory.Exists(name) == false)
            {
                Directory.CreateDirectory(name);
            }
        }

        /// <summary>
        /// Creates the specified sub-directory, if it doesn't exist.
        /// </summary>
        /// <param name="name">The name of the sub-directory to be created.</param>
        /// <param name="deleteIfExists">Indicates if the directory should be deleted if it exists.</param>
        internal static void CreateSubDirectory(string name, bool deleteIfExists)
        {
            if (Directory.Exists(name))
            {
                Directory.Delete(name, true);
            }

            Directory.CreateDirectory(name);
        }

        private static string GetFileFullPath(string path, string filename, bool isNetworkFile = false)
        {
            return isNetworkFile
                ? path + "\\" + filename
                : HttpContext.Current.Server.MapPath(HttpContext.Current.Request.ApplicationPath + "//" + path + "//" + filename);
        }
        //public static void DownloadNetworkFile(string fileName, string networkPath, string customName = "")
        //{
        //    Utilities.Network.NetworkDrive networkDrive = new Utilities.Network.NetworkDrive();
        //    string username = System.Configuration.ConfigurationManager.AppSettings["WindowsUsername"];
        //    string password = System.Configuration.ConfigurationManager.AppSettings["WindowsPassword"];
        //    networkDrive.MapNetworkDrive(networkPath, "", username, password);
        //    var fileInBytes = File.ReadAllBytes(networkPath + "\\" + fileName);
        //    string fileExt = Path.GetExtension(fileName).ToLower();
        //    HttpContext.Current.Response.Clear();
        //    HttpContext.Current.Response.ContentType = "application/octet-stream";
        //    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + (!string.IsNullOrEmpty(customName) ? customName.Replace(" ", "_") + fileExt : fileName));
        //    HttpContext.Current.Response.BinaryWrite(fileInBytes);
        //    HttpContext.Current.Response.End();            
        //}

        public static void DownloadNetworkFile(string fileName, string networkPath, string customName = "")
        {
            string fileExt = Path.GetExtension(fileName).ToLower();
            Stream fileToDownload = null;
            try
            {
                fileToDownload = new FileStream(networkPath + "\\" + fileName, FileMode.Open, FileAccess.Read);
                int bufferLength = Convert.ToInt32(fileToDownload.Length);
                byte[] buffer = new byte[bufferLength];
                int length = 0;
                do
                {
                    if (HttpContext.Current.Response.IsClientConnected)
                    {
                        length = fileToDownload.Read(buffer, 0, bufferLength);
                        HttpContext.Current.Response.ContentType = "application/octet-stream";
                        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + (!string.IsNullOrEmpty(customName) ? customName.Replace(" ", "_") + fileExt : fileName));
                        HttpContext.Current.Response.OutputStream.Write(buffer, 0, length);
                        buffer = new byte[bufferLength];
                    }
                    else
                    {
                        length = -1;
                    }
                }
                while (length > 0);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            finally
            {
                if (fileToDownload != null)
                    fileToDownload.Close();
            }
        }

        public static bool DownloadFile(string fileVirtualPath, string saveFileAs)
        {
            try
            {
                FileInfo fileToDownload = new FileInfo(HttpContext.Current.Server.MapPath(fileVirtualPath));
                if (fileToDownload.Exists)
                {
                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.ContentType = "application/octet-stream";
                    HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + saveFileAs.Replace(" ", "_") + fileToDownload.Extension);
                    //HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + fileToDownload.Name);
                    HttpContext.Current.Response.WriteFile(fileToDownload.FullName);
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        //public static bool DownloadFile(string currentFileName, string path, string newFileName)
        //{
        //    try
        //    {
        //        FileInfo fileToDownload = new FileInfo(HttpContext.Current.Server.MapPath(HttpContext.Current.Request.ApplicationPath + "//" + path + "//" + currentFileName));
        //        if (fileToDownload.Exists)
        //        {
        //            HttpContext.Current.Response.Clear();
        //            HttpContext.Current.Response.ContentType = "application/octet-stream";
        //            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + newFileName.Replace(" ", "_") + fileToDownload.Extension);
        //            HttpContext.Current.Response.WriteFile(fileToDownload.FullName);
        //            HttpContext.Current.ApplicationInstance.CompleteRequest();
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}

        public static bool IsValidFileType(string fileName, string[] fileTypes)
        {
            string validTypes = string.Join("|", fileTypes);
            var pattern = "^.*\\.(" + validTypes.ToLower() + "|" + validTypes.ToUpper() + ")$";
            return Regex.IsMatch(fileName, pattern, RegexOptions.IgnoreCase);
        }

        #region MVC Methods
        /// <summary>
        /// Upload file to specific folder 
        /// </summary>
        /// <param name="postedFile">File Upload control that contains the file path in the client pc</param>
        /// <param name="oldFileName">The file name we want to delete</param>
        /// <param name="path">Upload path on the server</param>
        /// <param name="maxFileSize">Maximum allowed file size in bytes</param>
        /// <param name="fileTypes">array of allowed file types</param>
        /// <returns>UploadResult object</returns>
        public static UploadResult UploadFile(HttpPostedFileWrapper postedFile, string oldFileName, string path, int maxFileSize, string[] fileTypes)
        {
            var result = new UploadResult();
            try
            {
                if (postedFile == null || postedFile.ContentLength == 0)
                {
                    result.Status = UploadStatus.Empty;
                }
                else if (!IsValidFileType(postedFile.FileName, fileTypes))
                {
                    result.Message = string.Format("The file \"{0}\" is not a valid type. Only {1} files are allowed", postedFile.FileName, string.Join(", ", fileTypes));
                    //result.Message = string.Format("نوع الملف \"{0}\" غير صحيح.الملفات المسموح بها فقط من نوع {1}", postedFile.FileName, string.Join(", ", fileTypes));
                    result.Status = UploadStatus.Error;
                }
                else if (postedFile.ContentLength > maxFileSize)
                {
                    result.Message = string.Format("The file \"{0}\" exceeds the maximum size limit of {1} MB", postedFile.FileName, (maxFileSize / 1024 / 1024));
                    //result.Message = string.Format("الملف \"{0}\" تجاوز الحد الأقصى للحجم المسموح به وهو {1} ميجا", postedFile.FileName, (maxFileSize / 1024 / 1024));
                    result.Status = UploadStatus.Error;
                }
                else
                {
                    string uploadedFileName;
                    if (UploadFile(postedFile, path, out uploadedFileName))
                    {
                        result.UploadPath = path;
                        result.OldFileName = oldFileName;
                        result.UploadedFileName = uploadedFileName;
                        result.Status = UploadStatus.Success;
                    }
                    else
                    {
                        result.Message = string.Format("An error occurred while uploading the file \"{0}\"", postedFile.FileName);
                        //result.Message = string.Format("حدث خطأ أثناء رفع الملف \"{0}\"", postedFile.FileName);
                        result.Status = UploadStatus.Error;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.LogError();
                result.Message = string.Format("An error occurred while uploading the file \"{0}\"", postedFile.FileName);
                //result.Message = string.Format("حدث خطأ أثناء رفع الملف \"{0}\"", postedFile.FileName);
                result.Status = UploadStatus.Error;
            }
            return result;
        }

        /// <summary>
        /// Upload file to specific folder 
        /// </summary>
        /// <param name="postedFile">File Upload control that contains the file path in the client pc</param> 
        /// <param name="path">Upload path on the server</param>
        /// <param name="maxFileSize">Maximum allowed file size in bytes</param>
        /// <param name="fileTypes">array of allowed file types</param>
        /// <returns>UploadResult object</returns>
        public static UploadResult UploadFile(HttpPostedFileWrapper postedFile, string path, int maxFileSize, string[] fileTypes)
        {
            return UploadFile(postedFile, null, path, maxFileSize, fileTypes);
        }
        #endregion
    }
}
