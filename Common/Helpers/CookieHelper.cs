﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;

namespace Common
{
    public class CookieHelper
    {
        //----------------------------------------------------------------------------------------------------------------------------------------------
        public static void RemoveKey(string key)
        {
            var httpCookie = HttpContext.Current.Response.Cookies[key];
            if (httpCookie != null)
            {
                httpCookie.Expires = DateTime.Now.AddDays(-1);
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------------------
        public static void AddValue(string key, string value)
        {
            RemoveKey(key);
            HttpContext.Current.Response.Cookies.Add(new HttpCookie(key, value)
            {
                HttpOnly = true,//Setting the HttpOnly value to true, makes this cookie accessible only to ASP.NET.
                Path = System.Web.Security.FormsAuthentication.FormsCookiePath
            });
        }

        //----------------------------------------------------------------------------------------------------------------------------------------------
        public static void RemoveAll()
        {
            foreach (string key in HttpContext.Current.Request.Cookies.AllKeys)
            {
                RemoveKey(key);
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------------------
        public static bool CheckKey(string key)
        {
            return (HttpContext.Current.Request.Cookies[key] != null);
        }
        //----------------------------------------------------------------------------------------------------------------------------------------------
        public static string GetValue(string key)
        {
            var httpCookie = HttpContext.Current.Request.Cookies[key];
            return httpCookie != null ? httpCookie.Value : null;
        }
        //----------------------------------------------------------------------------------------------------------------------------------------------
    }
}
