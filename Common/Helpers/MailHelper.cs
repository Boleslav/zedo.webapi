﻿using System;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace Common
{
    public class MailHelper
    {
        /// <summary>
        /// Mail Result used in send mail return value
        /// to return to params (Bool,String)
        /// </summary>
        public struct MailResult
        {
            /// <summary>
            /// Constructors to initialize the values of the strucr 
            /// </summary>
            /// <param name="res">bool result value</param>
            /// <param name="msg">string message value</param>
            public MailResult(bool res, string msg)
            {
                this.message = msg;
                this.result = res;
            }

            public bool result;
            public string message;

            /// <summary>
            /// ToString method return in this format "result,Message"
            /// </summary>
            /// <returns>return result,Messgae</returns>
            public override string ToString()
            {
                return (String.Format("{0},{1}", result, message));

            }
        }

        /// <summary>
        /// Send Mail With Features, full control message object
        /// </summary>
        /// <param name="message">Mail Message Object Contain All Message Details</param>
        /// <param name="mailSMTP">Mail SMTP information</param>
        /// <returns></returns>
        public static MailResult SendCustomMail(MailMessage message, SmtpClient mailSMTP)
        {
            try
            {
                MailResult ms = new MailResult(false, string.Empty);
                mailSMTP.Send(message);
                ms.message = "Mail Sent successfully";
                ms.result = true;
                return ms;

            }
            catch (Exception ex)
            {
                MailResult ms = new MailResult(false, string.Empty);
                ms.message = ex.Message;
                ms.result = false;
                return ms;
            }
        }

        /// <summary>
        /// Send Mail To One Recipient
        /// Return : Bool message sent, string message
        /// </summary>
        /// <param name="from">Mail From</param>
        /// <param name="to">Recipient Mail</param>
        /// <param name="subject">Mail Subject</param>
        /// <param name="body">Mail Message Body</param>
        /// <param name="mailSMTP">Mail SMTP information</param>
        /// <returns>Bool message sent, string message</returns>
        public static MailResult SendMail(MailAddress from, MailAddress to, string subject, string body, SmtpClient mailSMTP)
        {
            try
            {
                MailResult ms = new MailResult(false, string.Empty);

                MailMessage message = new MailMessage();
                message.From = from;
                message.To.Add(to);
                message.Subject = subject;
                message.Body = body;
                mailSMTP.Send(message);
                ms.message = "Mail Sent successfully";
                ms.result = true;
                return ms;
            }
            catch (Exception ex)
            {
                MailResult ms = new MailResult(false, ex.Message);
                return ms;
            }
        }

        /// <summary>
        /// Send Mail To One Recipient
        /// Return : Bool message sent, string message
        /// </summary>
        /// <param name="from">Mail From</param>
        /// <param name="to">Recipient Mail</param>
        /// <param name="subject">Mail Subject</param>
        /// <param name="body">Mail Message Body</param>
        /// <param name="HTMLBody">Define if the mail is body is HTML</param>
        /// <param name="priority">Mail Priority</param>
        /// <param name="mailSMTP">Mail SMTP information</param>
        /// <returns>Bool message sent, string message</returns>
        public static MailResult SendMail(MailAddress from, MailAddress to, string subject, string body, bool HTMLBody, MailPriority priority, SmtpClient mailSMTP)
        {
            try
            {
                MailResult ms = new MailResult(false, string.Empty);

                MailMessage message = new MailMessage();
                message.From = from;
                message.To.Add(to);
                message.Subject = subject;
                message.Body = body;
                message.IsBodyHtml = HTMLBody;
                message.Priority = priority;
                mailSMTP.Send(message);
                ms.message = "Mail Sent successfully";
                ms.result = true;
                return ms;
            }
            catch (Exception ex)
            {
                MailResult ms = new MailResult(false, ex.Message);
                return ms;
            }
        }


        /// <summary>
        /// Send Mail To One Recipient
        /// Return : Bool message sent, string message
        /// </summary>
        /// <param name="from">Mail From</param>
        /// <param name="to">Recipient Mail</param>
        /// <param name="subject">Mail Subject</param>
        /// <param name="body">Mail Message Body</param>
        /// <param name="HTMLBody">Define if the mail is body is HTML</param>
        /// <param name="priority">Mail Priority</param>
        /// <param name="AttachFilePath">Attachment File Path</param>
        /// <param name="mailSMTP">Mail SMTP information</param>
        /// <returns>Bool message sent, string message</returns>
        public static MailResult SendMail(MailAddress from, MailAddress to, string subject, string body, bool HTMLBody, MailPriority priority, string AttachFilePath, SmtpClient mailSMTP)
        {
            try
            {
                MailResult ms = new MailResult(false, string.Empty);

                MailMessage message = new MailMessage();
                message.From = from;
                message.To.Add(to);
                message.Subject = subject;
                message.Body = body;
                message.IsBodyHtml = HTMLBody;
                message.Priority = priority;
                message.Attachments.Add(new Attachment(AttachFilePath));
                mailSMTP.Send(message);
                ms.message = "Mail Sent successfully";
                ms.result = true;
                return ms;
            }
            catch (Exception ex)
            {
                MailResult ms = new MailResult(false, ex.Message);
                return ms;
            }
        }


        /// <summary>
        /// Send Mail To Many Recipients
        /// Return : Bool message sent, string message
        /// </summary>
        /// <param name="from">Mail From</param>
        /// <param name="to">Recipients Mail Collection</param>
        /// <param name="subject">Mail Subject</param>
        /// <param name="body">Mail Message Body</param>
        /// <param name="mailSMTP">Mail SMTP information</param>
        /// <returns>Bool message sent, string message</returns>
        public static MailResult SendMailToMany(MailAddress from, MailAddressCollection to, string subject, string body, SmtpClient mailSMTP)
        {
            try
            {
                MailResult ms = new MailResult(false, string.Empty);

                MailMessage message = new MailMessage();
                message.From = from;
                foreach (MailAddress ma in to)
                {
                    message.To.Add(ma);
                }
                message.Subject = subject;
                message.Body = body;
                mailSMTP.Send(message);
                ms.message = "Mail Sent successfully";
                ms.result = true;
                return ms;
            }
            catch (Exception ex)
            {
                MailResult ms = new MailResult(false, ex.Message);
                return ms;
            }
        }


        /// <summary>
        /// Send Mail To Many Recipients
        /// Return : Bool message sent, string message
        /// </summary>
        /// <param name="from">Mail From</param>
        /// <param name="to">Recipients Mail Collection</param>
        /// <param name="subject">Mail Subject</param>
        /// <param name="body">Mail Message Body</param>
        /// <param name="HTMLBody">Define if the mail is body is HTML</param>
        /// <param name="priority">Mail Priority</param>
        /// <param name="mailSMTP">Mail SMTP information</param>
        /// <returns>Bool message sent, string message</returns>
        public static MailResult SendMailToMany(MailAddress from, MailAddressCollection to, string subject, string body, bool HTMLBody, MailPriority priority, SmtpClient mailSMTP)
        {
            try
            {
                MailResult ms = new MailResult(false, string.Empty);

                MailMessage message = new MailMessage();
                message.From = from;
                message.To.Add(from);//added by tamer
                foreach (MailAddress ma in to)
                {
                    message.Bcc.Add(ma);
                    //message.To.Add(ma);
                }
                message.Subject = subject;
                message.Body = body;
                message.IsBodyHtml = HTMLBody;
                message.Priority = priority;
                mailSMTP.Send(message);
                ms.message = "Mail Sent successfully";
                ms.result = true;
                return ms;
            }
            catch (Exception ex)
            {
                MailResult ms = new MailResult(false, ex.Message);
                return ms;
            }
        }

        /// <summary>
        /// Send Mail To Many Recipients
        /// Return : Bool message sent, string message
        /// </summary>
        /// <param name="from">Mail From</param>
        /// <param name="to">Recipients Mail Collection</param>
        /// <param name="subject">Mail Subject</param>
        /// <param name="body">Mail Message Body</param>
        /// <param name="HTMLBody">Define if the mail is body is HTML</param>
        /// <param name="priority">Mail Priority</param>
        /// <param name="AttachFilePath">Attachment File Path</param>
        /// <param name="mailSMTP">Mail SMTP information</param>
        /// <returns>Bool message sent, string message</returns>
        public static MailResult SendMailToMany(MailAddress from, MailAddressCollection to, string subject, string body, bool HTMLBody, MailPriority priority, string AttachFilePath, SmtpClient mailSMTP)
        {
            try
            {
                MailResult ms = new MailResult(false, string.Empty);

                MailMessage message = new MailMessage();
                message.From = from;
                foreach (MailAddress ma in to)
                {
                    message.To.Add(ma);
                }
                message.Subject = subject;
                message.Body = body;
                message.IsBodyHtml = HTMLBody;
                message.Priority = priority;
                message.Attachments.Add(new Attachment(AttachFilePath));
                mailSMTP.Send(message);
                ms.message = "Mail Sent successfully";
                ms.result = true;
                return ms;
            }
            catch (Exception ex)
            {
                MailResult ms = new MailResult(false, ex.Message);
                return ms;
            }
        }

        public static bool IsEmailAddress(string input)
        {
            return !string.IsNullOrWhiteSpace(input) && Regex.IsMatch(input, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", RegexOptions.IgnoreCase);
        }
    }
}
