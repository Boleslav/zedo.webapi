﻿using System;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Common
{
    public static class IdentityHelper
    {
        public static LoggedInUser User
        {
            get
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    var userIdentity = HttpContext.Current.User.Identity as UserIdentity;
                    return userIdentity != null ? userIdentity.User : null;
                }
                return null;
            }
        }
        

        private static void CreateTicket(string name, short expirationInMinutes = 60, bool isPersistent = false, string userData = "")
        {
            var authenticationTicket = new FormsAuthenticationTicket(1, name, DateTime.Now, DateTime.Now.AddMinutes(expirationInMinutes), isPersistent, userData);
            HttpContext.Current.Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(authenticationTicket)));
        }

        public static void SetAuthCookie(string name, bool rememberMe, LoggedInUser loggedInUser = null)
        {
            string userData = loggedInUser != null
                                ? JsonConvert.SerializeObject(loggedInUser, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore })
                                : string.Empty;
            
            CreateTicket(name, isPersistent: rememberMe, userData: userData);
        }

        public static void RequestAuthentication()
        {
            var authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            
            if (authCookie != null)
            {
                var authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                if (authTicket != null)
                {
                    var userIdentity = new UserIdentity(authTicket);
                    userIdentity.User = JsonConvert.DeserializeObject<LoggedInUser>(authTicket.UserData);
                    string[] userRoles = new string[] { userIdentity.User?.RoleId.ToString()};
                    HttpContext.Current.User = new GenericPrincipal(userIdentity, userRoles);
                }
            }
        }

        public static void LogOut()
        {
            HttpContext.Current.Session.Abandon();
            CookieHelper.RemoveAll();
            FormsAuthentication.SignOut();
        }
    }

    public class LoggedInUser
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public string UserName { get; set; }
        public bool IsBackEnd { get; set; }
        public string Email { get; set; }
    }

    

    public class UserIdentity : IIdentity, IPrincipal
    {
        private readonly FormsAuthenticationTicket _authTicket;

        public UserIdentity(FormsAuthenticationTicket authTicket)
        {
            _authTicket = authTicket;
        }

        public string AuthenticationType
        {
            get { return "User"; }
        }

        public bool IsAuthenticated
        {
            get { return true; }
        }

        public string Name
        {
            get { return _authTicket.Name; }
        }

        public string UserId
        {
            get { return _authTicket.UserData; }
        }

        public bool IsInRole(string role)
        {
            return Roles.IsUserInRole(role);
        }

        
        public IIdentity Identity
        {
            get { return this; }
        }

        #region Custom Properties
        public LoggedInUser User { get; set; }
        #endregion
    }
}
