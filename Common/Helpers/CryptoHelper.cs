/*******************************
 * Encryption Class 'CryptBase'
 * Version : 2.0
 * Developer : Mohamed El-Bably
 *  
 * Features:
 * 3 Symmetric-key encryption algorithms
 * Password hashing with SHA or MD5
 * 
 * v 2.0 Changes:
 * PasswordDeriveBytes.GetBytes() This method is now obsolete.
 ******************************/

using System;
using System.IO;
using System.Security.Cryptography;

/// <summary>
/// Summary description for Crypto
/// </summary>

namespace Common
{
    public class CryptBase
    {
        /// <summary>
        /// Encryption Type
        /// </summary>
        public enum EncType
        {
            /// <summary>
            /// "Ron's Code (RC)" Standard max key size = 56 bits, 
            /// Ms Enhanced ver 128 bits
            /// </summary>
            RC2,
            /// <summary>
            /// "Data Encryption Standard (DES)", 
            /// Max key size = 56
            /// </summary>
            DES,
            /// <summary>
            /// "Triple DES", 
            /// Max key size = 168
            /// </summary>
            TripleDES
        }

        /// <summary>
        /// Hashing Algorithms
        /// </summary>
        public enum HashType
        {
            /// <summary>
            /// "Secure Hash Algorithm (SHA)"
            /// </summary>
            SHA,
            /// <summary>
            /// "Message-Digest algorithm 5 (MD5)"
            /// </summary>
            MD5
        }

        private HashType passwordHash = HashType.SHA;
        private EncType encryptionType = EncType.RC2;
        private string password;

        public EncType EncryptionType
        {
            get { return encryptionType; }
            set { encryptionType = value; }
        }

        public HashType PasswordHash
        {
            get { return passwordHash; }
            set { passwordHash = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }


        /// <summary>
        /// Encrypt string with password
        /// </summary>
        /// <param name="strText">String you want to encrypt</param>
        /// <returns>Encryptd string</returns>
        public string Encrypt(string strText)
        {
            int kLen = 128;
            string buffer = null;

            byte[] IV = { 0x4d, 0x30, 0xff, 0x8a, 0x33, 0x12, 0xaf, 0x8a };
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(password, new byte[] { 0x20, 0x4f });

            if (encryptionType != EncType.RC2)
            {
                byte e = ((byte)encryptionType);
                kLen = (int)(Math.Pow(2, e) - 1) * 56;
            }

            byte[] bykey = pdb.CryptDeriveKey(encryptionType.ToString(), passwordHash.ToString(), kLen, IV);
            byte[] InputByteArray = System.Text.Encoding.UTF8.GetBytes(strText);

            try
            {
                SymmetricAlgorithm sA = null;

                switch (encryptionType)
                {
                    case EncType.RC2:
                        sA = RC2CryptoServiceProvider.Create();
                        break;
                    case EncType.DES:
                        sA = DESCryptoServiceProvider.Create();
                        break;
                    case EncType.TripleDES:
                        sA = TripleDESCryptoServiceProvider.Create();
                        break;
                    default:
                        break;
                }

                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, sA.CreateEncryptor(bykey, IV), CryptoStreamMode.Write);

                cs.Write(InputByteArray, 0, InputByteArray.Length);
                cs.Close();

                buffer = Convert.ToBase64String(ms.ToArray());

                pdb.Reset();
                sA.Clear();
                cs.Dispose();
                ms.Dispose();

                return buffer;
            }
            catch
            {
                // return ex.Message;
                return null;
            }
        }

        /// <summary>
        /// Decrypt string.
        /// </summary>
        /// <param name="strText">Encrypted string</param>
        /// <returns>Decrypted string</returns>
        public string Decrypt(string strText)
        {
            int kLen = 128;
            string buffer = null;

            byte[] IV = { 0x4d, 0x30, 0xff, 0x8a, 0x33, 0x12, 0xaf, 0x8a };
            byte[] inputByteArray = new byte[strText.Length + 1];
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password, new byte[] { 0x20, 0x4f });

            if (encryptionType != EncType.RC2)
            {
                byte e = ((byte)encryptionType);
                kLen = (int)(Math.Pow(2, e) - 1) * 56;
            }

            byte[] bykey = pdb.CryptDeriveKey(encryptionType.ToString(), passwordHash.ToString(), kLen, IV);
            byte[] InputByteArray = System.Text.Encoding.UTF8.GetBytes(strText);

            try
            {
                SymmetricAlgorithm sA = null;

                switch (encryptionType)
                {
                    case EncType.RC2:
                        sA = RC2CryptoServiceProvider.Create();
                        break;
                    case EncType.DES:
                        sA = DESCryptoServiceProvider.Create();
                        break;
                    case EncType.TripleDES:
                        sA = TripleDESCryptoServiceProvider.Create();
                        break;
                    default:
                        break;
                }


                inputByteArray = Convert.FromBase64String(strText);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, sA.CreateDecryptor(bykey, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.Close();

                System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                buffer = encoding.GetString(ms.ToArray());

                pdb.Reset();
                sA.Clear();
                cs.Dispose();
                ms.Dispose();

                return buffer;
            }
            catch
            {
                //return ex.Message;
                return null;
            }
        }
    }
}
