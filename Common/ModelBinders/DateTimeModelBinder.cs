﻿using System;
using System.Globalization;
using System.Web.Mvc;

namespace Common
{
    public class DateTimeModelBinder : System.Web.Mvc.DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, System.Web.Mvc.ModelBindingContext bindingContext)
        {
            var displayFormat = bindingContext.ModelMetadata.DisplayFormatString;
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

            if (!string.IsNullOrEmpty(displayFormat) && value != null)
            {
                DateTime date;
                displayFormat = displayFormat.Replace("{0:", string.Empty).Replace("}", string.Empty);
                // use the format specified in the DisplayFormat attribute to parse the date
                if (DateTime.TryParseExact(value.AttemptedValue, displayFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    return date;
                }
                else if (!string.IsNullOrWhiteSpace(value.AttemptedValue))
                {
                    bindingContext.ModelState.AddModelError(bindingContext.ModelName, $"{bindingContext.ModelMetadata.DisplayName} is invalid");
                }
            }
            return base.BindModel(controllerContext, bindingContext);
        }
    }
}