﻿using System.Web;
using Common;
using System.Text;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.Collections;
using System.Configuration;

namespace System
{
    #region Enums
    #endregion
    /// <summary>
    /// <para>Parse Extension Contain Common Methods</para>
    /// <para>For Try Parse String To Other Types as Extension Method</para>
    /// </summary>
    public static class Extensions
    {
        const string DateFormat = "dd/MM/yyyy";
        private static readonly string SiteUrl = ConfigurationManager.AppSettings.Get("SiteUrl");
        #region Numeric Methods
        /// <summary>
        /// Converts the string representation of a number to byte
        /// returns : byte from parsed string if succeed or 0 if fail
        /// </summary>
        /// <param name="text">String contains number to convert</param>
        /// <returns>byte from parsed string if succeed or Exception if fail</returns>
        public static byte ToByte(this string text)
        {
            byte result;
            if (byte.TryParse(text, out result))
            {
                return result;
            }
            throw new Exception("Invalid Byte Number");
        }

        /// <summary>
        /// Converts the string representation of a number to its 64-bit
        /// returns : int64 from parsed string if succeed or -1 if fail
        /// </summary>
        /// <param name="text">String contains number to convert</param>
        /// <returns>int64 from parsed string if succeed or Exception if fail</returns>
        public static long ToLong(this string text)
        {
            long result;
            if (long.TryParse(text, out result))
            {
                return result;
            }
            throw new Exception("Invalid Long Number");
        }

        /// <summary>
        /// Converts the string representation of a number to its double
        /// returns : double from parsed string if succeed or -1 if fail
        /// </summary>
        /// <param name="text">String contains number to convert</param>
        /// <returns>double from parsed string if succeed or Exception if fail</returns>
        public static double ToDouble(this string text)
        {
            double result;
            if (double.TryParse(text, out result))
            {
                return result;
            }
            throw new Exception("Invalid Double Number");
        }

        /// <summary>
        /// Converts the string representation of a number to its 32-bit
        /// returns : int32 from parsed string if succeed or -1 if fail
        /// </summary>
        /// <param name="text">String contains number to convert</param>
        /// <returns>int32 from parsed string if succeed or Exception if fail</returns>
        public static int ToInt(this string text)
        {
            int result;
            if (int.TryParse(text, out result))
            {
                return result;
            }
            throw new Exception("Invalid Integer Number");
        }

        /// <summary>
        /// Converts the string representation of a number to its integer
        /// returns : True from parsed string if succeed or false if fail
        /// </summary>
        /// <param name="text">String contains number to convert</param>
        /// <param name="result">integer of parsed string</param>
        /// <returns>True from parsed string if succeed or false if fail</returns>
        public static bool IsInt(this string text, out int result)
        {
            return int.TryParse(text, out result);
        }

        /// <summary>
        /// Converts the string representation of a number to its integer
        /// returns : True from parsed string if succeed or false if fail
        /// </summary>
        /// <param name="text">String contains number to convert</param> 
        /// <returns>True from parsed string if succeed or false if fail</returns>
        public static bool IsInt(this string text)
        {
            int result;
            return text.IsInt(out result);
        }

        /// <summary>
        /// Converts the string representation of a number to its decimal
        /// returns : decimal from parsed string if succeed or -1 if fail
        /// </summary>
        /// <param name="text">String contains number to convert</param>
        /// <returns>decimal from parsed string if succeed or Exception if fail</returns>
        public static decimal ToDecimal(this string text)
        {
            decimal result;
            if (decimal.TryParse(text, out result))
            {
                return result;
            }
            throw new Exception("Invalid Decimal Number");
        }

        /// <summary>
        /// Converts the string representation of a number to its decimal
        /// returns : True from parsed string if succeed or false if fail
        /// </summary>
        /// <param name="text">String contains number to convert</param>
        /// <param name="result">Decimal of parsed string</param>
        /// <returns>True from parsed string if succeed or false if fail</returns>
        public static bool IsDecimal(this string text, out decimal result)
        {
            return decimal.TryParse(text, out result);
        }

        /// <summary>
        /// Converts the string representation of a number to its decimal
        /// returns : True from parsed string if succeed or false if fail
        /// </summary>
        /// <param name="text">String contains number to convert</param> 
        /// <returns>True from parsed string if succeed or false if fail</returns>
        public static bool IsDecimal(this string text)
        {
            decimal result;
            return text.IsDecimal(out result);
        }
        #endregion

        #region String Methods
        /// <summary>
        /// Converts the string representation of a Plain Text to HTML
        /// returns : HTML from parsed string if succeed or empty string if fail
        /// </summary>
        /// <param name="text">Plain Text to convert</param>
        /// <returns>HTML from parsed string if succeed or empty string if fail</returns>
        public static string ToHtml(this string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                StringBuilder htmlBuilder = new StringBuilder(text);
                return htmlBuilder.Replace(Environment.NewLine, "<br/>").ToString();
            }
            return string.Empty;
        }

        /// <summary>
        /// Remove Html tags and convert to Plain Text 
        /// returns : Plain Text
        /// </summary>
        /// <param name="text">Html text to convert</param>
        /// <returns>Plain Text</returns>
        public static string ToPlainText(this string text)
        {
            return !string.IsNullOrWhiteSpace(text) ? Regex.Replace(HttpUtility.HtmlDecode(text), "<.*?>", string.Empty) : string.Empty;
        }

        /// <summary>
        /// Get QueryString value from string Url
        /// </summary>
        /// <param name="urlText">Url string text</param>
        /// <param name="key">QueryString key</param>
        /// <returns>Value associated with the specified key</returns>
        public static string GetQueryString(this string urlText, string key)
        {
            return HttpUtility.ParseQueryString(new Uri(urlText).Query).Get(key);
        }

        /// <summary>
        /// Converts the gaven virtual file path (e.g: ~/Uploads/filename.ext) to the absolute path (e.g: http://wwww.domain.com/Uploads/filename.ext)
        /// </summary>
        /// <param name="fileVirtualPath">Virtual file path (e.g: ~/Uploads/filename.ext)</param>
        /// <returns>Absolute file path</returns>
        public static string ToAbsolutePath(this string fileVirtualPath)
        {
            return !string.IsNullOrWhiteSpace(fileVirtualPath) ? HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + VirtualPathUtility.ToAbsolute(fileVirtualPath) : string.Empty;
        }

        public static string ToJson(this IEnumerable objectToSerialize)
        {
            //JsonConvert.SerializeObject(objectToSerialize, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
            using (var stringWriter = new StringWriter())
            {
                using (var writer = new JsonTextWriter(stringWriter) { QuoteName = false })
                    new JsonSerializer().Serialize(writer, objectToSerialize);
                return stringWriter.ToString();
            }
        }

        public static string ToVirtualPath(this string urlText)
        {
            return !string.IsNullOrWhiteSpace(urlText) ? urlText.Replace("~", SiteUrl) : urlText;
        }
        #endregion

        #region Date Methods
        /* /// <summary>
         /// Converts the string representation of a datetime to its Datetime
         /// returns : Datetime from parsed string if succeed or Exception if fail
         /// </summary>
         /// <param name="text">String contains datetime to convert</param>
         /// <returns>decimal from parsed string if succeed or -1 if fail</returns>
         public static DateTime ToDate(this string text)
         {
             DateTime result;
             if (DateTime.TryParse(text, out result))
             {
                 return result;
             }
             throw new Exception("Invalid Datetime");
         }*/

        /// <summary>
        /// Converts the string representation of a datetime to its Datetime by the given format
        /// returns : Datetime from parsed string if succeed or Exception if fail
        /// </summary>
        /// <param name="text">String contains datetime to convert</param>
        /// <param name="format">the format of datetime to parse with</param>
        /// <returns>decimal from parsed string if succeed or -1 if fail</returns>
        public static DateTime ToDate(this string text, string format = DateFormat)
        {
            DateTime result;
            if (DateTime.TryParseExact(text, format, null, DateTimeStyles.None, out result))
            {
                return result;
            }
            else
            {
                throw new Exception("Invalid Datetime");
            }
        }

        /// <summary>
        /// Converts the string representation of a datetime to its Datetime by the given format
        /// returns : True from parsed string if succeed or false if fail
        /// </summary>
        /// <param name="text">String contains datetime to convert</param>
        /// <param name="format">the format of datetime to parse with</param>
        /// <param name="result">DateTime of parsed string</param>
        /// <returns>True from parsed string if succeed or false if fail</returns>
        public static bool IsDate(this string text, string format, out DateTime result)
        {
            return DateTime.TryParseExact(text, format, null, DateTimeStyles.None, out result);
        }

        /// <summary>
        /// Converts the string representation of a datetime to its Datetime by the given format
        /// returns : True from parsed string if succeed or false if fail
        /// </summary>
        /// <param name="text">String contains datetime to convert</param>
        /// <param name="format">the format of datetime to parse with</param> 
        /// <returns>True from parsed string if succeed or false if fail</returns>
        public static bool IsDate(this string text, string format = DateFormat)
        {
            DateTime result;
            return text.IsDate(format, out result);
        }

        /// <summary>
        /// Converts datetime to InvariantCulture datetime
        /// returns : String date in InvariantCulture format
        /// </summary>
        /// <param name="date">Datetime to convert</param>
        /// <returns>returns : String date in InvariantCulture format</returns>
        public static string ToInvariantDate(this DateTime date)
        {
            return date.ToString(CultureInfo.InvariantCulture);
        }
        #endregion

        #region Security Methods
        /// <summary>
        /// Encrypt the passed string 
        /// </summary>
        /// <param name="text">Text to encrypt</param>
        /// <returns>Encrypted string</returns>
        public static string Encrypt(this string text)
        {
            try
            {
                CryptBase crypto = new CryptBase();
                crypto.EncryptionType = CryptBase.EncType.TripleDES;
                crypto.PasswordHash = CryptBase.HashType.SHA;
                crypto.Password = Configuration.ConfigurationManager.AppSettings["EncryptionKey"];
                return crypto.Encrypt(text);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Decrypt the passed string 
        /// </summary>
        /// <param name="text">Text to decrypt</param>
        /// <returns>Original string text</returns>
        public static string Decrypt(this string text)
        {
            try
            {
                CryptBase crypto = new CryptBase();
                crypto.EncryptionType = CryptBase.EncType.TripleDES;
                crypto.PasswordHash = CryptBase.HashType.SHA;
                crypto.Password = Configuration.ConfigurationManager.AppSettings["EncryptionKey"];
                return crypto.Decrypt(text);
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region LogMethods
        public static void LogError(this Exception exception)
        {
            try
            {
                while (exception != null)
                {
                    var dirLogs = new DirectoryInfo(HttpContext.Current.Server.MapPath("~/Logs"));
                    if (!dirLogs.Exists)
                    {
                        dirLogs.Create();
                    }

                    var logBuilder = new StringBuilder();
                    logBuilder.AppendLine("Error Date and Time : ");
                    logBuilder.AppendLine(DateTime.Now.ToString("dd/MM/yyyy hh:mm tt"));
                    logBuilder.AppendLine();
                    logBuilder.AppendLine("Message : ");
                    logBuilder.AppendLine(exception.Message);
                    logBuilder.AppendLine();
                    logBuilder.AppendLine("Source : ");
                    logBuilder.AppendLine(exception.Source);
                    logBuilder.AppendLine();
                    logBuilder.AppendLine("StackTrace : ");
                    logBuilder.AppendLine(exception.StackTrace);
                    logBuilder.AppendLine();
                    logBuilder.AppendLine("-------------------------------------------------------------");
                    logBuilder.AppendLine();

                    File.AppendAllText(string.Format(@"{0}\Log_{1}.txt", dirLogs.FullName, DateTime.Now.ToString("dd_MM_yyyy")), logBuilder.ToString());
                    exception = exception.InnerException;
                }
            }
            catch
            {
                // 
            }
        }
        #endregion 

        #region MVC Methods 
        public static T GetController<T>(this WebViewPage currentView) where T : Controller
        {
            return currentView.ViewContext.Controller as T;
        }

        public static bool GetCheckboxValue(this FormCollection formCollection, string checkboxId)
        {
            try
            {
                return Convert.ToBoolean(formCollection[checkboxId].Split(',')[0]);
            }
            catch (Exception)
            {
                return false;
            }
        }
        //public static T CastTo<T>(this Web.WebPages.HelperResult helper) where T : struct
        //{
        //    var result = Convert.ChangeType(helper.ToString().Trim(), typeof(T));
        //    return (T)result;
        //}
        #endregion
    }
}