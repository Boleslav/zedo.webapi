﻿namespace Common
{
    public class Enums
    {
        public enum Color
        {
            Red = 1,
            Blue = 2,
            Yellow = 3,
            Green = 4
        }
        public enum LetterStatus : byte
        {
            New = 1,
            InProgress = 2,
            Rejected = 3,
            Approved = 4,
            Archived = 5
        }

        public enum SportTypes : byte
        {
            FootballFirstTeam = 1,
            FootballYouth = 2,
            FootballCompanies = 3,
            Basketball = 4,
            Volleyball = 5,
            Handball = 6,
            Hallball = 7,
            Tennis = 8,
            Swimming = 9,
            Athletics = 10
        }

        public enum RoleTypes
        {
            Level1,
            Level2,
            Level3,
            Level4,
            Admin,
        }

        public enum LetterTypes : byte
        {
            Outgoing = 1,
            Incoming = 2 
        }
    }
}
